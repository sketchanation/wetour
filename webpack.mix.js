const mix = require('laravel-mix')

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('resources/sass/app.scss', 'public/css')
    .sass('resources/sass/argon/argon.scss', 'public/css/theme.css')
    .scripts(
        [
            // 'resources/js/turbino/jquery-3.1.1.min.js',
            // 'resources/js/turbino/bootstrap.min.js',
            // 'resources/js/turbino/popper.min.js',
            // 'resources/js/turbino/jquery.magnific-popup.min.js',
            // 'resources/js/turbino/smooth-scroll.min.js',
            // 'resources/js/turbino/instafeed.min.js',
            // 'resources/js/turbino/ofi.js',
            // 'resources/js/turbino/jquery-ui.js',
            // // 'resources/js/turbino/initslider.js',
            // 'resources/js/turbino/jquery.ui.touch-punch.min.js',
            // 'resources/js/turbino/select2.min.js',
            // 'resources/js/turbino/main.js'

            'resources/vendor/jquery/jquery.min.js',
            'resources/vendor/popper/popper.min.js',
            'resources/vendor/bootstrap/bootstrap.min.js',
            'resources/vendor/headroom/headroom.min.js',
            'resources/vendor/onscreen/onscreen.min.js',
            'resources/vendor/nouislider/js/nouislider.min.js',
            'resources/vendor/nouislider/js/wNumb.js',
            'resources/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
            'resources/js/argon/argon.min.js'
        ],
        'public/js/theme.js'
    )
    .copyDirectory('resources/img/', 'public/img')
    .copyDirectory('resources/vendor/', 'public/vendor')
    .copyDirectory('resources/svgs/', 'public/svgs')
    //dashboard
    .js('resources/js/app.js', 'public/js')
    .sass('resources/sass/dashboard/index.scss', 'public/css/dashboard.css')
    .styles(
        [
            'node_modules/vuetify/dist/vuetify.min.css',
            'node_modules/chartist/dist/chartist.min.css'
        ],
        'public/css/dashboardVuetify.css'
    )
