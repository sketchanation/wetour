/**
 * Define all of your application routes here
 * for more information on routes, see the
 * official documentation https://router.vuejs.org/en/
 */
import Translate from '../views/Tours/Translate.vue'

export default [
    {
        path: '/',
        // Relative to /src/views
        view: 'Tours/Index'
    },
    {
        path: '/tours',
        // Relative to /src/views
        view: 'Tours/Index'
    },
    {
        path: '/tours/create',
        view: 'Tours/Create'
    },

    {
        path: '/tours/:id',
        view: 'Tours/Edit'
    },
    {
        path: '/tours/:id/translate',
        name: 'tour-translate',
        view: 'Tours/Translate'
    },

    {
        path: '/features',
        view: 'Features'
    },

    {
        path: '/tags',
        view: 'Tags'
    },

    {
        path: '/locations',
        view: 'Locations'
    },

    {
        path: '/reservations',
        view: 'Reservations/Index'
    },

    {
        path: '/reservations/group',
        view: 'Reservations/Group'
    },

    {
        path: '/reservations/:id',
        view: 'Reservations/View'
    },

    {
        path: '/users',
        view: 'Users'
    },

    {
        path: '/points',
        view: 'Points'
    },

    {
        path: '/calendar',
        view: 'Calendar',
        meta: { role: 'Guide' }
    },

    {
        path: '/daytimes',
        view: 'DayTimes'
    },
    {
        path: '/grouptypes',
        view: 'GroupTypes'
    },

    {
        path: '/user-profile',
        name: 'User Profile',
        view: 'UserProfile'
    },
    {
        path: '/table-list',
        name: 'Table List',
        view: 'TableList'
    },
    {
        path: '/typography',
        view: 'Typography'
    },
    {
        path: '/icons',
        view: 'Icons'
    },
    {
        path: '/maps',
        view: 'Maps'
    },
    {
        path: '/notifications',
        view: 'Notifications'
    },
    {
        path: '/upgrade',
        name: 'Upgrade to PRO',
        view: 'Upgrade'
    }
]
