/**
 * Vue Router
 *
 * @library
 *
 * https://router.vuejs.org/en/
 */

// Lib imports
import Vue from 'vue'
// import VueAnalytics from 'vue-analytics'
import Router from 'vue-router'
// import Meta from 'vue-meta'

import NProgress from 'nprogress'

// Routes
import paths from './paths'

function route(path, view, name, meta, children) {
    return {
        name: name || view,
        path,
        meta,
        children,
        component: resovle => import(`../views/${view}.vue`).then(resovle)
    }
}

Vue.use(Router)

// Create a new router
const router = new Router({
    mode: 'history',
    base: 'dashboard',
    routes: paths
        .map(path =>
            route(path.path, path.view, path.name, path.meta, path.children)
        )
        .concat([{ path: '*', redirect: '/' }]),
    scrollBehavior(to, from, savedPosition) {
        if (savedPosition) {
            return savedPosition
        }
        if (to.hash) {
            return { selector: to.hash }
        }
        return { x: 0, y: 0 }
    }
})

// Vue.use(Meta)

// Bootstrap Analytics
// Set in .env
// https://github.com/MatteoGabriele/vue-analytics
// if (process.env.GOOGLE_ANALYTICS) {
//     Vue.use(VueAnalytics, {
//         id: process.env.GOOGLE_ANALYTICS,
//         router,
//         autoTracking: {
//             page: process.env.NODE_ENV !== 'development'
//         }
//     })
// }

router.beforeResolve((to, from, next) => {
    if (to.name) {
        NProgress.start({ showSpinner: false })
    }
    next()
})
router.afterEach((to, from) => {
    NProgress.done(true)
})

router.beforeEach((to, from, next) => {
    axios.get('/me').then(x => {
        this.a.app.$store.commit('updateUser', x.data.data)
    })
    next()
})

export default router
