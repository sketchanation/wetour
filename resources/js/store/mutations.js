// https://vuex.vuejs.org/en/mutations.html

export default {
  snack (state, payload) {
    state.snack = true
    state.message = payload.message
    state.type = payload.type
    state.icon = payload.icon
    setTimeout(() => {
      state.snack = false
    }, 3000);
  },
  updateUser (state, user) {
    state.user = user
  }
}
