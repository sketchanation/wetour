import Vue from "vue";
import { Validator } from "vee-validate";
import { mapGetters } from "vuex";

const globals = {
  install(Vue, Options) {
    Vue.mixin({
      computed: {
        ...mapGetters(["user"]),
      },
      data(){
      return {
        csrf:null
      }
    },
    created() {
        this.csrf = document.head.querySelector(
            "[name='csrf-token'][content]"
          ).content;
    },
      methods: {
        ////////// vvalidate locale
        localize (localeName) {
          // localize your app here, like i18n plugin.
          // asynchronously load the locale file then localize the validator with it.
          import(`vee-validate/dist/locale/${localeName}`).then(locale => {
            Validator.localize(localeName, locale);
          })
        },
      },
    });
  }
};

Vue.use(globals)
export default globals;
