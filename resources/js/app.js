/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap')

window.Vue = require('vue')

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'))

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key)))

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import Vuetify from 'vuetify'
import router from './router'
import store from './store'
import i18n from './i18n'

import App from './Main'
import mixins from './mixins'
import './register'
import '@mdi/font/css/materialdesignicons.css'

Vue.use(Vuetify, {
    iconfont: 'mdi',
    theme: {
        primary: '#4caf50',
        secondary: '#4caf50',
        tertiary: '#495057',
        accent: '#82B1FF',
        error: '#f55a4e',
        info: '#00d3ee',
        success: '#5cb860',
        warning: '#ffa21a'
    }
})
Vue.use(require('vue-chartist'))

import VeeValidate from 'vee-validate'
import localeEs from 'vee-validate/dist/locale/es'

import VueUploadComponent from 'vue-upload-component'
Vue.component('file-upload', VueUploadComponent)

import VueFormWizard from 'vue-form-wizard'
import 'vue-form-wizard/dist/vue-form-wizard.min.css'
Vue.use(VueFormWizard)

Vue.use(VeeValidate, {
    events: 'change',
    locale: 'es',
    dictionary: {
        es: localeEs
    }
})
Vue.component('file-upload', VueUploadComponent)

import Vue from 'vue'
import VueQuillEditor from 'vue-quill-editor'

// require styles
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
// import 'quill/dist/quill.bubble.css'

Vue.use(VueQuillEditor, {
    // debug: 'info',
    modules: {
        toolbar: [
            ['bold', 'italic', 'underline', 'strike'],
            ['blockquote', 'code-block'],
            [{ header: 1 }, { header: 2 }],
            [{ list: 'ordered' }, { list: 'bullet' }],
            // [{ 'script': 'sub' }, { 'script': 'super' }],
            // [{ 'indent': '-1' }, { 'indent': '+1' }],
            // [{ 'direction': 'rtl' }],
            [{ size: ['small', false, 'large', 'huge'] }],
            [{ header: [1, 2, 3, 4, 5, 6, false] }],
            [{ font: [] }],
            [{ color: [] }, { background: [] }],
            [{ align: [] }],
            ['clean'],
            [
                'link'
                // , 'image', 'video'
            ]
        ]
    },
    placeholder: '...',
    // readOnly: true,
    theme: 'snow'
})

// import wysiwyg from "vue-wysiwyg";
// import "vue-wysiwyg/dist/vueWysiwyg.css";
// Vue.use(wysiwyg, {
// { [module]: boolean (set true to hide) }
// hideModules: { "bold": true },

// you can override icons too, if desired
// just keep in mind that you may need custom styles in your application to get everything to align
// iconOverrides: { "bold": "<i class='your-custom-icon'></i>" },

// if the image option is not set, images are inserted as base64
// image: {
//     uploadURL: "/upload/image",
//    dropzoneOptions: {}
// },

// limit content height if you wish. If not set, editor size will grow with content.
// maxHeight: "500px",

// set to 'true' this will insert plain text without styling when you paste something into the editor.
// forcePlainTextOnPaste: true
// });

import * as VueGoogleMaps from 'vue2-google-maps'

Vue.use(VueGoogleMaps, {
    load: {
        key: 'AIzaSyBCil315ckCpRGr2lKuUhZOEk0cwHJSbHE',
        libraries: 'places' // necessary for places input
    }
})

const app = new Vue({
    el: '#app',
    render: h => h(App),
    router,
    store,
    i18n
})
