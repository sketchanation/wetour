<div id="errorNotification" class="col-12 notification normal-notification hide">
        <div class="">
            <div class="alert alert-danger">        
            @foreach ( $errors->all() as $error )
                @if( $loop->first )
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
                        <span class="alert-inner--text"><i class="fa fa-exclamation-triangle"></i> We have encounter some errors!</span>
                @endif
                        {{ $error }} <br>
            @endforeach
            </div>
        </div>
    </div>