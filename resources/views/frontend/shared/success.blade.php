<div id="successNotification" class="col-12 notification normal-notification hide">
        <div class="">
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <span class="alert-inner--text"><i class="fa  fa-check-circle-o"></i> {{ session('success') }}</span>
            </div>
        </div>
    </div>