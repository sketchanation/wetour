<div id="warningNotification" class="col-12 notification normal-notification hide">
        <div class="">
            <div class="alert alert-warning">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            <span class="alert-inner--text"><i class="fa fa-spin fa-cog"></i> {{ session('warning') }}</span>
            </div>
        </div>
    </div>