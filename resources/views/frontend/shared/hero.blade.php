@if (!isset($body))
    <div class="tour-title not-fixed ">
    <img class="w-100 h-100" src="{{$src ? $src : '/img/bg4.jpg' }}" alt="">
    <h1 class="white text-center front shadow-text center-text">{{$title}}</h1>  
    </div>
@else
    {{$body}}
@endif
