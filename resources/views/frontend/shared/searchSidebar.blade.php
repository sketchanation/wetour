<div class="form-container py-2 border">
<h4 class="black bold mt-3 px-4 pb-2 text-center">{{__('Filter your Destination')}}</h4>
    <form id="sidebar-form" class="px-xl-5 px-lg-3 px-4" method="GET" action="{{route('search')}}"> 
        <div class="form-group row">
        <div class="col-sm-12">
                <div class="input-group">
                <input type="text" class="form-control" id="name" name="search"  value="{{request('search') ?: ''}}" placeholder="{{__('Search Tours')}}">
                        <div class="input-group-append">
                            <div class="input-group-text"><i class="fa fa-search"></i>
                            </div>
                        </div>
                    </div>
        </div>

        <div class="col-sm-12">
                <div class="input-group mt-3">
                        <select class="form-control "  name="type" id="inlineFormInputName2">
                                <option value="" selected>{{__('common.Type')}}</option>
                                    @foreach ($group_types as $group)
                                <option value="{{$group->id}}" {{request('type') == $group->id ? 'selected' :  ''}}>{{studly_case($group->name)}}</option>
                                    @endforeach
                                </select>
                                <div class="input-group-append">
                                        <div class="input-group-text"><i class="fa fa-user"></i></div>
                                    </div>
                </div>
           </div>

           <div class="col-sm-12">
               <div class="input-group mt-3">
                    <select class="  form-control " name="day_time" id="inlineFormInputName3">
                            <option value="" selected>{{__('common.Time')}}</option>
                                @foreach ($times as $time)
                            <option value="{{$time->id}}" {{request('day_time') == $time->id ? 'selected' : ''}}>{{studly_case($time->name)}}</option>
                                @endforeach
                            </select>
                            <div class="input-group-append">
                                    <div class="input-group-text"><i class="fa fa-clock"></i></div>
                                </div>
               </div>
           </div>

        </div>
        <div class="form-group row">
            <div class="col-sm-12">
                <div class="input-group">
                    {{-- <select class="form-control" id="group_types" name="group_types" multiple>
                        @forelse ($group_types as $type)
                            @if ($loop->first)
                                <option selected="">Tour Type</option>                                                        
                            @endif
                            <option value="{{$type->id}}" class="text-capitalize">{{$type->name}}</option>
                            
                        @empty
                            
                        @endforelse
                    </select> --}}
                    
                </div>                                         
            </div>
        </div>   
      
        {{-- @if ($tours->count() > 1) --}}
        <div class="form-group row">
                <div class="col-sm-12">       
                     <div class="d-block">
                     <p class=" text-center" >
                     <label  class="text-center" for="amount">{{__('Price range')}}:</label>                                 
                    <input  class="text-center" hidden name="price" type="text" id="amount" readonly >
                    </p>
                    </div>
                <div id="slider-range"></div>
                </div>
            </div>
        {{-- @endif --}}

        <div class="form-group row">
        <div class="col-sm-12">
        <button type="submit" class="btn col-sm-12 my-2  btn-outline-primary">{{__('common.Search')}}</button>

            </div>
        </div>
    </form>
    </div>

    @section('scripts')
    
 <script defer>

var range = document.getElementById('slider-range');
var slider = noUiSlider.create(range, {

    range: {
        'min': {{$min}},
        'max': {{$max}}
    },

    step: 20,

    // Handles start at ...
    start: [
           {{request('price') ?  intval(  str_replace('$','',explode(',' , request('price'))[0])  ) :   $min}}, 
           {{request('price') ?  intval(  str_replace('$','',explode(',' , request('price'))[1])  ) :   $max}} 
           ],


    // Display colored bars between handles
    connect: true,

    

    // Move handle on tap, bars are draggable
    behaviour: 'tap-drag',
    tooltips: true,
    format: wNumb({
        decimals: 0,
        // thousand: '.',
        suffix: ' $'
    })

   
})

range.noUiSlider.on("update", function(a, b) {
            $('#amount').val(range.noUiSlider.get())
        });
                </script>
@endsection