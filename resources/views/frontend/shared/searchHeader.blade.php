<div id="chapter1" class="chapter" >

    <div class="container text-center justify-content-center align-items-center searchform">
    <h3 class="white mx-2 my-3 text-center shadow-text d-block">{{__('Find the place it suits you best')}}</h3>    
        <form method="GET" action="{{route('search')}}"  class="form-inline text-center justify-content-center align-items-center ">
        <input type="text" name="search" value="{{request('search') ?: ''}}"  class="form-control-inline2 form-control mb-2 mr-lg-2 mx-md-0 mx-4 py-2" id="inlineFormInputName1" placeholder="{{__('Search Tours')}}"> 
            <span class="fas fa-search iconform"></span>

            <select class="mb-2 mr-lg-2 mx-4 mx-md-0 form-control form-control-inline2" value="" name="type" id="inlineFormInputName2">
            <option value="" selected>{{__('common.Type')}}</option>
                @foreach ($group_types as $group)
            <option value="{{$group->id}}" {{request('type') == $group->id ? 'selected' :  ''}}>{{studly_case($group->name)}}</option>
                @endforeach
            </select>
            <span class="fas fa-calendar iconform"></span>
            <select class=" mb-2 mr-lg-2 mx-4 mx-md-0 form-control form-control-inline2" name="day_time" id="inlineFormInputName3">
            <option value="" selected>{{__('common.Time')}}</option>
                @foreach ($times as $time)
            <option value="{{$time->id}}" {{request('day_time') == $time->id ? 'selected' : ''}}>{{studly_case($time->name)}}</option>
                @endforeach
            </select>
            <span class="fas fa-chevron-down iconform"></span>

        <button type="submit" class="btn btn-primary mb-2 mx-4 mx-md-0 mr-lg-2 py-2 form-control-inline3">{{__('common.Search')}}</button>

        </form>
    </div>
    <div class="chapter2">
    </div>
</div>