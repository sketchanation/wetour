@extends('frontend.layouts.app')

@section('content')
<main>
    <div class="position-relative">
        <section class="section section-lg section-hero section-shaped">
            <!-- Background circles -->
            <div class="shape bakanto-shape-home"></div>
            <div class="container shape-container d-flex align-items-center py-md">
                <div class="col px-0">
                    <div class="row align-items-center justify-content-center">
                        <div class="col-lg-8 text-center">
                            <p class="lead text-white bakanto-text-home">Find the tours that suit you</p>
                            <form method="GET" action="{{route('search')}}">
                            <div class="d-flex bd-highlight mt-5">
                                  <div class="p-2 flex-fill bd-highlight">
                                      <input type="text" name="search" value="{{request('search') ?: ''}}"  class="form-control" placeholder="{{__('Search Tours')}}"> 
                                  </div>
                                  <div class="p-2 flex-fill bd-highlight">
                                      <select class="form-control" name="type">
                                                                  <option value="" selected>{{__('common.All')}}</option>
                                                                  @foreach ($group_types as $group)
                                                                  <option value="{{$group->id}}" {{request('type') == $group->id ? 'selected' :  ''}}>{{studly_case($group->name)}}</option>
                                                                      @endforeach
                                                          </select>
                                  </div>
                                  <div class="p-2 flex-fill bd-highlight">
                                      <select class="form-control" name="day_time">
                                          <option value="" selected>{{__('common.All')}}</option>
                                          @foreach ($times as $time)
                                          <option value="{{$time->id}}" {{request('day_time') == $time->id ? 'selected' : ''}}>{{studly_case($time->name)}}</option>
                                              @endforeach
                                  </select>
                                  </div>
                                  <div class="p-2 flex-fill bd-highlight">
                                      <button type="submit"  class="btn btn-lg btn-search btn-icon mb-3 mb-sm-0" style="padding: .74rem 1rem;">
                                          <span class="btn-inner--icon"><i class="fa fa-search"></i></span>
                                          <span class="btn-inner--text">
                                              <span>SEARCH</span>
                                          </span>
                                        </button>
                                  </div>
                            </div>
                          </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <section class="section">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="row justify-content-center text-center">
                        <div class="col-lg-8">
                            <h1 class="display-3">Explore this tours</h1>
                            <h2 class="display-4">Best & More Popular Tours</h2>
                            <hr style="width: 30px; height: 4px; background-color: #fb6340; display: block; margin: 15px auto 54px;">
                        </div>
                    </div>
                    <div class="row row-grid">
                        @foreach ($tours->take(6) as $tour)
                        <div class="col-lg-4 mb-4">
                            <div class="card card-lift--hover shadow border-0">
                            <img src="{{$tour->featured_img}}" class="card-img-top bakanto-img-top">
                                <div class="card-body py-3">
                                <h5 class="text-uppercase">{{$tour->name}}</h5>
                                <p>{{$tour->excerpt}}</p>
                                <p><strong>from {{$tour->price}}€</strong> <small class="text-muted">per person</small></p>
                                    <div>
                                        @foreach ($tour->tags as $tag)
                                    <span class="badge badge-pill badge-warning">{{$tag->name}}</span>
                                        @endforeach
                                    </div>
                                  <a href="{{route('tour.show',$tour)}}" class="btn btn-warning btn-block mt-4">Learn more</a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section pt-0">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="row justify-content-center text-center">
                        <div class="col-lg-8">
                            <h1 class="display-3">Top Destinations</h1>
                            <hr style="width: 30px; height: 4px; background-color: #fb6340; display: block; margin: 15px auto 54px;">
                        </div>
                    </div>
                    <div class="row row-grid">
                        @foreach ($locations->shuffle()->take(6) as $location)
                        <div class="col-lg-4 mb-4">
                        <a href="/location/{{$location->id}}">
                                    <div class="card bg-default shadow border-0">
                                    <img src="{{$location->img}}" style="max-height:220px" class="card-img-top">
                                            <div class="card-img-overlay bakanto-shadow d-flex">
                                                <div class="my-auto mx-auto text-center">
                                                <h6 class="display-4 text text-white">{{$location->name}}</h6>
                                                </div>
                                            </div>
                                        </div>
                               </a>
                            </div>
                        @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection