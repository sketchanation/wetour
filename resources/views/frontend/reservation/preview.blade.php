@extends('frontend.layouts.app')

@section('content')
<section class="section section-md section-shaped">
    <div class="shape bakanto-shape-home" style="background-image: url('http://bakanto.com/img/aboutus3.jpg'); background-position: bottom center;"></div>
    <div class="container shape-container d-flex align-items-center py-md">
        <div class="col px-0">
            <div class="row align-items-center justify-content-center">
                <div class="col-lg-8 text-center">
                    <p class="text-white bakanto-text-home">BOOK YOUR TOUR</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section ">
    <div class="container">
       <div class="row" >
            <form style="justify-content:center" id="form" action="{{route('reservation.pay', request()->route('tour') )}}" method="POST" class="row">
                    @csrf

           <div class="col-8">
                <div class="card   shadow border-0">
                        <div class="card-body">
                                <div class="row">
                                        {{-- @if ( session()->has(['date', 'day_time', 'persons', 'price']) ) --}}
                                      <h5 class="text-center col-12 mb-3" style="margin:0 auto">  Reservation due {{ session('date')}}  in the {{ session('day_time') }}  <br>
                                            Include's  {{ session('persons')}} persons <br>
                                            Total price: {{ session('price') }} $ (USD) </h5>

                                            @if (session('persons') > 1)
                                                    @for ($i = 0; $i < session('persons') - 1; $i++)
                                                    <div class="col-md-4 mt-2">
                                                        <input class="form-control" type="text" name="extras_name[]" value="{{old('extras_name.' . $i)}}" id="extra_name_{{$i}}" required placeholder="Acompanion #{{$i+1}} name"  >
                                                    </div>
                                                    <div class="col-md-4 mt-2">
                                                    <input class="form-control" type="text" name="extras_email[]" value="{{old('extras_email.' . $i)}}" id="extra_email_{{$i}}" required placeholder="Acompanion #{{$i+1}} email"  >
                                                    </div>
                                                    <div class="col-md-4 mt-2">
                                                    <input class="form-control" type="number" name="extras_age[]" value="{{old('extras_age.' . $i)}}" id="extra_age_{{$i}}" min="1" max="100" required placeholder="Acompanion #{{$i+1}} age"  >
                                                    </div>
                                                @endfor
                                            @endif

                                            <div class="col-12 mt-3">
                                            <textarea name="comment" class="form-control" placeholder="Any extra comments?">{{old('comment')}}</textarea>
                                            </div>
                                            <div class="col-12 mt-3">
                                              <p>
                                                    I've read and understood the <a href="#">Terms and Services</a> 
                                              <input type="checkbox" checked="{{old('terms')}}" name="terms">
                                              </p>
                                                    </div>


                                                    
                                </div>
                        </div>
                    </div>
                    
                    <div class="card  shadow-lg mt-4 border-0">
                        <div class="card-body row">
                                
                    
                            <div class="col-12">
                                <label for="card-element">
                                    Credit or debit card
                                  </label>
                                  <div id="card-element">
                                    <!-- A Stripe Element will be inserted here. -->
                                  </div>
                              
                                  <!-- Used to display form errors. -->
                                  <div id="card-errors" role="alert"></div>
                            </div>
                                
                        </div>
                    </div>
           </div>


           <div class="col-4">
            <div class="card card-lift--hover shadow border-0">
                <img src="{{$tour->featured_img}}" class="card-img-top bakanto-img-top">
                    <div class="card-body py-3">
                    <h5 class="text-uppercase">{{$tour->name}}</h5>
                    <p>{{$tour->excerpt}}</p>
                    <p><strong>from {{$tour->price}}€</strong> <small class="text-muted">per person</small></p>
                        <div>
                            @foreach ($tour->tags as $tag)
                            <span class="badge badge-pill badge-warning">{{$tag->name}}</span>
                            @endforeach
                        </div>
                    <a href="{{route('tour.show',$tour)}}" class="btn btn-warning btn-block mt-4">Learn more</a>
                    </div>
                </div>
       </div>


           
          <div class="col-12 mt-3">
                <a href="{{session()->previousUrl()}}" class="btn btn-outline">Go back.</a>
                <button type="submit" class="btn btn-primary float-right">Ready?</button>
                {{-- @else
                Something has gone wrong, please go back and try again later <a href="{{session()->previousUrl()}}">Go back</a>            
                @endif --}}
          </div>
        </form>    

       </div>
       
    </div>
</section>

@endsection

@section('scripts')
<script src="https://js.stripe.com/v3/"></script>
    <script>
    // Create a Stripe client.
var stripe = Stripe("{{env('STRIPE_KEY')}}");

// Create an instance of Elements.
var elements = stripe.elements({
    locale: '{{app()->getLocale()}}'
});

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
var style = {
  base: {
    color: '#32325d',
    lineHeight: '18px',
    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
    fontSmoothing: 'antialiased',
    fontSize: '16px',
    '::placeholder': {
      color: '#aab7c4'
    }
  },
  invalid: {
    color: '#fa755a',
    iconColor: '#fa755a'
  }
};

// Create an instance of the card Element.
var card = elements.create('card', {style: style});

// Add an instance of the card Element into the `card-element` <div>.
card.mount('#card-element');

// Handle real-time validation errors from the card Element.
card.addEventListener('change', function(event) {
  var displayError = document.getElementById('card-errors');
  if (event.error) {
    displayError.textContent = event.error.message;
  } else {
    displayError.textContent = '';
  }
});

// Handle form submission.
var form = document.getElementById('form');
form.addEventListener('submit', function(event) {
  event.preventDefault();

  stripe.createToken(card).then(function(result) {
    if (result.error) {
      // Inform the user if there was an error.
      var errorElement = document.getElementById('card-errors');
      errorElement.textContent = result.error.message;
    } else {
      // Send the token to your server.
      stripeTokenHandler(result.token);
    }
  });
});

// Submit the form with the token ID.
function stripeTokenHandler(token) {
  // Insert the token ID into the form so it gets submitted to the server
  var form = document.getElementById('form');
  var hiddenInput = document.createElement('input');
  hiddenInput.setAttribute('type', 'hidden');
  hiddenInput.setAttribute('name', 'stripeToken');
  hiddenInput.setAttribute('value', token.id);
  form.appendChild(hiddenInput);

  // Submit the form
  form.submit();
}</script>
@endsection