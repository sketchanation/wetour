@extends('frontend.layouts.app')

@section('content')
<section class="section section-md section-shaped">
        <div class="shape bakanto-shape-home" style="background-image: url('http://bakanto.com/img/aboutus3.jpg'); background-position: bottom center;"></div>
        <div class="container shape-container d-flex align-items-center py-md">
            <div class="col px-0">
                <div class="row align-items-center justify-content-center"> 
                    <div class="col-lg-8 text-center">
                        <p class="text-white bakanto-text-home">Reservation #{{$reservation->stripe_transaction_id}}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
<section class="section">
    <div class="container">
        @if ($reservation->trashed())
        <div class="alert alert-danger text-center">
        <h5>{{__('This reservation has been cancelled and refunded!')}}</h5>
        </div>
        @endif        
        <div class="row" style="justify-content:center">
            <div class="col-md-8">
                <div class="card shadow mb-2">
                <div class="card-header bg-primary border-0">
                        <h3 class="text-center">{{__('common.Reservation')}} {{$reservation->stripe_transaction_id}}!</h3>
                </div>
                <div class="card-body ">
                        <div class="row justify-content-center">
                                <div class="col-md-6">
                                        <p class="text-left">
                                                {{$reservation->user->name}}, {{__('you have been emailed the details at')}} <strong>{{$reservation->user->email}}</strong>
                                                
                                        <p>
                                </div>
                                <div class="col-md-6">
                                        <p>
                                                Your reservation for {{$reservation->getDateString()}} <br> 
                                                at the {{$reservation->day_time->name}}
                                                for {{$reservation->persons}} person(s) has been succesfully payed
                                        </p>
                                </div>
                                        
                                <div class="col-md-6">
                                        <p>
                                                {{__('Transaction details:')}} <br>
                                                {{__('Payed')}} ${{$reservation->price}} (USD) <br>
                                                {{__('Method: ')}} {{$reservation->pay_method}} <br>
                                                @if ($reservation->trashed())
                                                    {{__('This reservation has been refunded on the')}} <strong>{{$reservation->deleted_at->toDayDateTimeString()}}</strong> <br>
                                                    {{__('You have been emailed the details ')}}
                                                @endif
                                        </p>
                                </div>
                                <div class="col-md-6">
                                        <p>
                                                {{__('Extra comments')}}: {{$reservation->comment}}
                                        </p>
                                </div>
                                 @if (!$reservation->extras->isEmpty())
                                 <div class="col-md-7 text-center p-2">
                                        <h6>{{__('Accompanions')}}:</h6>
                                       <ul>
                                               @foreach ($reservation->extras as $extra)
                                                   <li>{{$extra->name}} / {{$extra->email}} / {{$extra->age}} </li>
                                               @endforeach
                                       </ul>
                               </div>
                                 @endif
                        </div>
                </div>
                </div>
            </div>
            <div class="col-8 mt-2">
            <a href="{{route('tour.show', $reservation->tour )}}" class="btn btn-primary float-right">{{__('View Tour')}}</a>
            </div>
        </div>

    </div>
</section>

@endsection