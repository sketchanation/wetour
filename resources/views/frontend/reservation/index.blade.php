@extends('frontend.layouts.app')

@section('content')
<section class="section section-md section-shaped">
    <div class="shape bakanto-shape-home" style="background-image: url('http://bakanto.com/img/aboutus3.jpg'); background-position: bottom center;"></div>
    <div class="container shape-container d-flex align-items-center py-md">
        <div class="col px-0">
            <div class="row align-items-center justify-content-center">
                <div class="col-lg-8 text-center">
                    <p class="text-white bakanto-text-home">Reservations</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section">
    <div class="container">
                <div class="row justify-content-center">
                    @forelse (auth()->user()->reservations()->withTrashed()->get() as $reservation)
                    <div class="col-md-7 p-2 my-3">
                    <div class="card">
                        <div class="card-header text-white bg-primary  ">
                            <h6 class="text-left">{{__('Reservation')}} {{$reservation->stripe_transaction_id}} 
                            @if($reservation->trashed())<span class="float-right badge badge-pill badge-danger ">{{__('Cancelled')}}</span> @endif</h6>   
                                
                        </div>
                        <div class="card-body ">
                            <div class="row">
                                <div class="col-md-6">
                                        <p>
                                            Date: <strong>{{$reservation->getDateString()}}</strong> <br> 
                                            In the <strong>{{$reservation->day_time->name}}</strong>
                                            for <strong>{{$reservation->persons}} person(s)</strong>
                                        </p>
                                </div>
                                <div class="col-md-6">
                                        <p>
                                            Total ${{$reservation->price}} (USD) <br>
                                            Method: {{$reservation->pay_method}} <br>
                                        </p>
                                </div>
                            </div>
                            <a class="btn float-right text-center btn-outline-primary" href="{{route('reservation.show', $reservation)}}">Check details.</a>
                        </div>
                    </div>
                    </div>
                @empty
                        
                    @endforelse
                </div>
    </div>
</section>

@endsection