@extends('frontend.layouts.app')

@section('content')
<main>
        <section class="section section-md section-shaped">
            <div class="shape bakanto-shape-home" style="background-image: url('http://bakanto.com/img/aboutus3.jpg'); background-position: bottom center;"></div>
            <div class="container shape-container d-flex align-items-center py-md">
                <div class="col px-0">
                    <div class="row align-items-center justify-content-center">
                        <div class="col-lg-8 text-center">
                            <p class="text-white bakanto-text-home">FIND YOUR TOUR</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section pb-0">
            <div class="container">
                <div class="row justify-content-center">
                    
                </div>
            </div>
        </section>
        <section class="section">
            <div class="container">
                <div class="row justify-content-center">
                        <div class="col-lg-3">
                                            @include('frontend.shared.searchSidebar')
                            </div>
                    <div class="col-lg-9">
                        <div class="row row-grid">
                            @forelse ($tours as $tour)
                            <div class="col-lg-4 mb-4">
                                    <div class="card card-lift--hover shadow border-0">
                                    <img src="{{$tour->featured_img}}" class="card-img-top bakanto-img-top">
                                        <div class="card-body py-3">
                                        <h5 class="text-uppercase">{{$tour->name}}</h5>
                                        <p>{{$tour->excerpt}}</p>
                                        <p><strong>from {{$tour->price}}€</strong> <small class="text-muted">per person</small></p>
                                            <div>
                                                @foreach ($tour->tags as $tag)
                                                <span class="badge badge-pill badge-warning">{{$tag->name}}</span>
                                                @endforeach
                                            </div>
                                        <a href="{{route('tour.show',$tour)}}" class="btn btn-warning btn-block mt-4">Learn more</a>
                                        </div>
                                    </div>
                                </div>
                                @empty
                                <h2 class="text-center warning-title col">No tour matches</h2>
                            @endforelse
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection