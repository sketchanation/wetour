@extends('frontend.layouts.app')

@section('content')
<main class="profile-page">
  <section class="section section-md section-shaped">
    <div class="shape bakanto-shape-home" style="background-image: url('http://bakanto.com/img/aboutus3.jpg'); background-position: bottom center;"></div>
    <div class="container shape-container d-flex align-items-center py-md">
        <div class="col px-0">
            <div class="row align-items-center justify-content-center">
                <div class="col-lg-8 text-center">
                    <p class="text-white bakanto-text-home">FIND YOUR TOUR</p>
                </div>
            </div>
        </div>
    </div>
</section>
    <section class="section">
      <div class="container">
        <div class="card card-profile shadow mt--300 mb-4">
          <div class="px-4">
            <div class="row justify-content-center">
              <div class="col-lg-3 order-lg-2">
                <div class="card-profile-image">
                  {{-- <a href="#">
                    <img src="/img/theme/team-4-800x800.jpg" class="rounded-circle">
                  </a> --}}
                </div>
              </div>
              <div class="col-lg-4 order-lg-3 text-lg-right align-self-lg-center">
                <div class="card-profile-actions py-4 mt-lg-0">
                    <a href="{{route('profile.edit')}}" class="btn btn-sm btn-info mr-4">{{__('common.Edit')}}</a>
                  {{-- <a href="#" class="btn btn-sm btn-default float-right">Message</a> --}}
                </div>
              </div>
              <div class="col-lg-4 order-lg-1">
                <div class="card-profile-stats d-flex justify-content-center">
                  {{-- <div>
                    <span class="heading">22</span>
                    <span class="description">Friends</span>
                  </div> --}}
                  {{-- <div>
                    <span class="heading">10</span>
                    <span class="description">Photos</span>
                  </div> --}}
                  {{-- <div>
                    <span class="heading">89</span>
                    <span class="description">Comments</span>
                  </div> --}}
                </div>
              </div>
            </div>
            <div class="text-center mt-5">
              <h3>{{$user->name}}
              </h3>
            <div class="h6 font-weight-300 muted"><i class="ni location_pin mr-2"></i>{{$user->created_at->diffForHumans()}}</div>
            <div class="h6 mt-4"><i class="ni business_briefcase-24 mr-2"></i>{{$user->email}}</div>
              {{-- <div><i class="ni education_hat mr-2"></i>University of Computer Science</div> --}}
            </div>
            <div class="mt-5 py-4 border-top text-center">
              <div class="row justify-content-center">
                <div class="col-lg-9">
                  <p>An artist of considerable range, Ryan — the name taken by Melbourne-raised, Brooklyn-based Nick Murphy — writes, performs and records all of his own music, giving it a warm, intimate feel with a solid groove structure. An artist of considerable range.</p>
                </div>
              </div>
            </div>
            <div class="mt-0 py-4 text-center">
                @foreach ($user->tags as $tag)
                <a href="#" class="btn btn-primary px-3 btn-sm " role="button">{{$tag->name}}</a>
            @endforeach
            </div>
          </div>
        </div> {{-- /card --}}
            <h2 class="display-4 text-center mx-2 muted">Recent Reservations</h2>
            @forelse ($user->getReservedTours()->take(5) as $tour)
            <div class="col-lg-4 mb-4">
              <div class="card card-lift--hover shadow border-0">
              <img src="{{$tour->featured_img}}" class="card-img-top bakanto-img-top">
                  <div class="card-body py-3">
                  <h5 class="text-uppercase">{{$tour->name}}</h5>
                  <p>{{$tour->excerpt}}</p>
                  <p><strong>from {{$tour->price}}€</strong> <small class="text-muted">per person</small></p>
                      <div>
                          @foreach ($tour->tags as $tag)
                          <span class="badge badge-pill badge-warning">{{$tag->name}}</span>
                          @endforeach
                      </div>
                  <a href="{{route('tour.show',$tour)}}" class="btn btn-warning btn-block mt-4">Check</a>
                  </div>
              </div>
          </div>
            @empty
              <div class="text-center">
                  <a class="btn btn-primary mt-3 " href="{{route('tour.index')}}" role="button">Top Tours</a>
              </div>
        @endforelse
       
      </div>
    </section>
  </main>
@endsection