@extends('frontend.layouts.app')

@section('content')
<section class="section section-md section-shaped">
    <div class="shape bakanto-shape-home" style="background-image: url('http://bakanto.com/img/aboutus3.jpg'); background-position: bottom center;"></div>
    <div class="container shape-container d-flex align-items-center py-md">
        <div class="col px-0">
            <div class="row align-items-center justify-content-center">
                <div class="col-lg-8 text-center">
                    <p class="text-white bakanto-text-home">Edit Profile</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section">
    <div class="container">
                <div class="row">
                    <div class="col-12 order-md-first order-last">
                            <div class="card shadow  mt--300 border-0">
                                <form id="contact-form" method="post" action="{{ route('profile.update') }}" role="form">
                                        @csrf
                                        @method('PUT')
                                        <div class="messages"></div>
                                        <div class="card-body">
                    
                                            <div class="row justify-center">
                                                <div class="col-md-6 col-xs-6">
                                                    <div class="form-group text-center">
                                                        <label for="name" class=" col-form-label ">{{ __('Name') }}</label>
                                                         <input id="name" type="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $user->name }}" required autofocus>
                                                    @if ($errors->has('name'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('name') }}</strong>
                                                        </span>
                                                    @endif
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-xs-6">
                                                    <div class="form-group text-center">
                                                        <label for="email" class=" col-form-label ">{{ __('E-Mail Address') }}</label>
                                                         <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $user->email }}" required autofocus>
                                                    @if ($errors->has('email'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('email') }}</strong>
                                                        </span>
                                                    @endif
                                                    </div>
                                                </div>
                                               
                                            </div>
                    
                                            <div class="row">
                                                <div class="col-md-6 col-xs-6 text-center">
                                                     <label for="password" class=" col-form-label ">{{ __('Password') }}</label>
                    
                                                      <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password">
                    
                                                    @if ($errors->has('password'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('password') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>

                                                <div class="col-md-6 col-xs-6 text-center">
                                                        <label for="password-confirm" class=" col-form-label ">{{ __('Confirm Password') }}</label>
                                                         <input id="password-confirm" type="password" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" name="password_confirmation">
                       
                                                       @if ($errors->has('password_confirmation'))
                                                           <span class="invalid-feedback" role="alert">
                                                               <strong>{{ $errors->first('password_confirmation') }}</strong>
                                                           </span>
                                                       @endif
                                                   </div>

                                                   <div class="col-12">
                                                        <label class=" col-form-label ">{{ __('common.Likes') }}</label>
                                                     <select multiple="multiple" class="form-control select2{{ $errors->has('likes[]') ? ' is-invalid' : '' }}" 
                                                        name="likes[]" >
                                                       @foreach ($tags as $tag)
                                                     <option class="btn btn-primary btn-sm" value="{{$tag->id}}" {{$user->tags->contains($tag) ? 'selected' : ''}} >{{$tag->name}}</option>
                                                       @endforeach
                                                     </select>
                                                   @if ($errors->has('likes[]'))
                                                       <span class="invalid-feedback" role="alert">
                                                           <strong>{{ $errors->first('likes[]') }}</strong>
                                                       </span>
                                                   @endif
                                               </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                <input type="submit" class="float-right btn btn-outline-primary px-4 btn-send btn-wide mt-3" value="{{__('common.Update')}}">
                                                <a  class="float-left btn btn-outline-primary px-4 btn-send btn-wide mt-3" href="{{route('profile')}}">{{__('common.Back')}}</a>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </form>
                                </div>
                    </div>

           
        </div>
      
    </div>
</section>
@endsection