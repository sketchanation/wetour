@extends('frontend.layouts.app')

@section('content')
<main>
        <section class="section section-md section-shaped">
            <div class="shape bakanto-shape-home" style="background-image: url('http://bakanto.com/img/aboutus3.jpg'); background-position: bottom center;"></div>
            <div class="container shape-container d-flex align-items-center py-md">
                <div class="col px-0">
                    <div class="row align-items-center justify-content-center">
                        <div class="col-lg-8 text-center">
                            <p class="text-white bakanto-text-home">FIND YOUR TOUR</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section pb-0">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-12">
                        <div class="card bg-gradient-secondary shadow">
                            <div class="card-body p-lg-3">
                                <div class="container">
                                    <form method="GET" action="{{route('search')}}">
                                            <div class="row">
                                                    <div class="col-md-4">
                                                        <small class="d-block text-uppercase font-weight-bold mb-3">Search Tours</small>
                                                        <input type="text" name="search" value="{{request('search') ?: ''}}"  class="form-control" placeholder="{{__('Search Tours')}}"> 
                                                    </div>
                                                    <div class="col-md-4">
                                                        <small class="d-block text-uppercase font-weight-bold mb-3">Type</small>
                                                        <select class="form-control" name="type">
                                                                <option value="" selected>{{__('common.All')}}</option>
                                                                @foreach ($group_types as $group)
                                                                <option value="{{$group->id}}" {{request('type') == $group->id ? 'selected' :  ''}}>{{studly_case($group->name)}}</option>
                                                                    @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <small class="d-block text-uppercase font-weight-bold mb-3">Time</small>
                                                        <select class="form-control" name="day_time">
                                                                <option value="" selected>{{__('common.All')}}</option>
                                                                @foreach ($times as $time)
                                                                <option value="{{$time->id}}" {{request('day_time') == $time->id ? 'selected' : ''}}>{{studly_case($time->name)}}</option>
                                                                    @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="row align-items-center justify-content-center">
                                                    <div class="col-md-6">
                                                        <button type="submit" class="btn btn-warning btn-block mt-4">SEARCH</button>
            
                                                    </div>
                                                </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-12">
                        <div class="row row-grid">
                            @forelse ($tours as $tour)
                            <div class="col-lg-4 mb-4">
                                    <div class="card card-lift--hover shadow border-0">
                                    <img src="{{$tour->featured_img}}" class="card-img-top bakanto-img-top">
                                        <div class="card-body py-3">
                                        <h5 class="text-uppercase">{{$tour->name}}</h5>
                                        <p>{{$tour->excerpt}}</p>
                                        <p><strong>from {{$tour->price}}€</strong> <small class="text-muted">per person</small></p>
                                            <div>
                                                @foreach ($tour->tags as $tag)
                                                <span class="badge badge-pill badge-warning">{{$tag->name}}</span>
                                                @endforeach
                                            </div>
                                        <a href="{{route('tour.show',$tour)}}" class="btn btn-warning btn-block mt-4">Learn more</a>
                                        </div>
                                    </div>
                                </div>
                                @empty
                                <h2 class="text-center warning-title col">No tour matches</h2>
                            @endforelse
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection