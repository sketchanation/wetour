@extends('frontend.layouts.app')

@section('content')
<main>
    <section class="section section-shaped section-lg">
      <div class="shape shape-style-1 bakanto-shape-home"></div>
      <div class="container pt-lg-md">
        <div class="row justify-content-center">
          <div class="col-lg-5">
            <div class="card bg-secondary shadow border-0">
              <div class="card-body px-lg-5 py-lg-5">
                <div class="text-center text-muted mb-4">
                  <h1 class="display-4">LOGIN</h1>
                </div>
                <form role="form" method="Post" action="{{ route('login') }}" >
                    @csrf
                  <div class="form-group">
                    <div class="input-group input-group-alternative mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                      </div>
                      <input  placeholder="{{ __('E-Mail Address') }}" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                      @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                      @endif
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="input-group input-group-alternative">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                      </div>
                      <input class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="{{ __('Password') }}" type="password" >
                      @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                    </div>
                  </div>
                  <div class="text-center">
                    <button type="submit" class="btn btn-warning mt-4">LOGIN</button>
                  </div>
                </form>
              </div>
            </div>
            <div class="row mt-3">
                <div class="col-6">
                  <a class="text-white" href="{{ route('password.request') }}">
                    <small>Forgot password?</small>
                  </a>
                </div>
                <div class="col-6 text-right">
                <a href="{{route('register')}}" class="text-white">
                    <small>Create new account</small>
                  </a>
                </div>
              </div>
          </div>
        </div>
      </div>
    </section>
  </main>
@endsection
