<div class="list-group" style="display: none;">
    <span class="list-group-item active">
        <img class="svgcenter mt-4 logo-light" src="/img/brand/logo_white.png" alt="image">
        <span class="pull-right" id="slide-submenu">
            <i class="fa fa-times"></i>
        </span>
    </span>
<h6 class="white py-4 px-5 text-center  list-group-item light">“{{__('common.Real guides for the price of a free tour')}}”</h6>
    <p class="white py-4 px-5 text-center  list-group-item light">
        {{__('common.Welcome to Bakanto My Tour, we got the best tours and amazing places waiting for you!')}}
    </p>
    <ul class="list-group-item py-4">
        <li>
            <h5 class="white text-center"><i class="white fas fa-map-marker-alt mr-2"></i>
                {{__('common.Avenue')}}, Madrid</h5>
        </li>
        <li>
            <h5 class="white text-center"><i class="white fas fa-phone-square mr-2"></i>Madrid, {{__('common.Spain')}} (+1) 3333.1111</h5>
        </li>
        <li>
            <h5 class="white text-center"><i class="white fas fa-envelope mr-2"></i>info@bakanto.com</h5>
        </li>
    </ul>
    <div class="list-group-item text-center pt-4 ">
    <h6>{{__('common.Follow Us')}}</h6>
        <ul class="text-center py-3">
            <li class="list-inline-item"><a href="http://www.facebook.com/bakantoMyTour/" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
            <li class="list-inline-item"><a href="http://www.twitter.com/bakantomytour/" target="_blank"><i class="fab fa-twitter"></i></a></li>
            <li class="list-inline-item"><a href="http://www.instagram.com/bakantomytour/" target="_blank"><i class="fab fa-instagram"></i></a></li>
        </ul>
        <div class="list-group-item py-4">
            <a href="/site/aboutus" class="d-block white py-2">
                <i class="fa fa-users"></i> {{__('common.About Us')}}
            </a>
            <a class="white d-block" href="/site/contact-us">
                <i class="fa fa-envelope"></i>{{__('common. Contact Us')}}
            </a>
        </div>
    </div>
</div>