<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>
        <!-- Favicon -->
        <link rel="icon" type="image/png" href="assets/images/favicon.png">

        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


        <!-- Seo meta tags for Search Engine Optimization -->
        <meta name="author" content="Myself">
        <meta name="description" content="  Welcome to Bakanto My Tour, we got the best tours and amazing places waiting for you!">
        <meta name="keywords" content="travel agency, tour agency, travel, adventure, destinations">
        <!-- Stylesheets -->
        <link href="{{ asset('img/brand/favicon.png') }}" rel="icon" type="image/png">
        <link href="{{ asset('vendor/nucleo/css/nucleo.css')}}" rel="stylesheet">
        <link href="{{ asset('vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
        <link href="{{ asset('css/theme.css') }}" rel="stylesheet">
    </head>

   <body>
    <!-- navbar -->
    @include('frontend.layouts.nav')
    <!-- /navbar -->

    {{-- @yield('hero') --}}

    @includeWhen($errors->any() , 'frontend.shared.errors')
    @includeWhen( session()->has('success'), 'frontend.shared.success')
    @includeWhen( session()->has('warning'), 'frontend.shared.warning')

    @yield('content')
     
        <!-- Footer -->
        @include('frontend.layouts.footer')
        <!-- End of Footer -->
        <script  src="{{ asset('js/theme.js') }}" ></script>
        @yield('scripts')
    </body>
</html> 