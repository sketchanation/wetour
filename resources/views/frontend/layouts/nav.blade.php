<header class="header-global">
    <nav id="navbar-main" class="navbar navbar-expand-lg navbar-transparent ">
      <div class="container">
        <a class="navbar-brand mr-lg-5" href="{{route('home')}}">
          <img src="/img/brand/logo-bakanto-white.png">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-collapse collapse" id="navbar_global">
          <div class="navbar-collapse-header">
            <div class="row">
              <div class="col-6 collapse-brand">
                <a href="{{route('home')}}">
                  <img src="/img/brand/blue.png">
                </a>
              </div>
              <div class="col-6 collapse-close">
                <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false" aria-label="Toggle navigation">
                  <span></span>
                  <span></span>
                </button>
              </div>
            </div>
          </div>
          <ul class="navbar-nav navbar-nav-hover ml-lg-auto">
              <li class="nav-item">
              <a class="nav-link nav-link-icon" href="{{route('tour.index')}}">
                      <span class="nav-link-inner--text">Tours</span>
                  </a>
              </li>
            @guest
            <li class="nav-item">
                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
            </li>
            @else
            @if (auth()->user()->hasRole('Customer'))
                        <li class="nav-item dropdown   mr-3 my-lg-0 my-2 ml-lg-0 ml-3">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        {{ auth()->user()->name }} <span class="caret"></span>
                                    </a>
                                    
                               <div class="dropdown-menu  mr-3 my-lg-0 my-2 ml-lg-0 ml-3" aria-labelledby="navbarDropdown">
                            <a  class="dropdown-item text-center"  href="{{ route('profile') }}">{{ __('Profile') }}</a>

                            @if (auth()->user()->getReservedTours()->count() > 0)
                            <a  class="dropdown-item text-center"  href="{{ route('reservation.index') }}">{{ __('Reservations') }}</a>
                            @endif

                            <a class="dropdown-item text-center" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                          document.getElementById('logout-form').submit();">
                             {{ __('Logout') }}
                         </a>

                         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                             @csrf
                         </form>
                        </div>
                        </li>
                        @else
                            <li class="nav-item dropdown   mr-3 my-lg-0 my-2 ml-lg-0 ml-3">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ auth()->user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu  mr-3 my-lg-0 my-2 ml-lg-0 ml-3" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item text-center" href="{{ route('dashboard') }}">{{__('common.Dashboard')}}</a>
                                     <div class="dropdown-divider"></div>
                                    <a class="dropdown-item text-center" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                            @endif
                        @endguest
            
            <li class="nav-item dropdown ">
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false" v-pre> <i class="fa fa-globe"></i>  <span class="caret"></span>
                </a>

                <div class="dropdown-menu  mr-3 my-lg-0 my-2 ml-lg-0 ml-3" aria-labelledby="navbarDropdown">
                    @foreach (config()->get('app.locales') as $locale)
                    <a class="dropdown-item text-center" href="{{ route('locale',$locale) }}">
                        {{strtoupper($locale)}}</a>
                    <div class="dropdown-divider"></div>
                    @endforeach
                
                </div>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  </header>