<footer class="footer">
    <div class="container">
        <div class="row row-grid align-items-center mb-5">
            <div class="col-lg-6">
                <ul class="list-unstyled quick-links">
                    <li>Contact Us</li>
                    <li>
                        <p class="white light">Have Questions? Our team is ready to help you!</p>
                    </li>
                    <li>
                        <h6><i class="fa fa-map-marker mr-2"></i>Madrid, Spain</h6>
                    </li>
                    <li>
                        <h6><i class="fa fa-phone-square mr-2"></i>Spain (+34) 3333.1111</h6>
                    </li>
                    <li>
                        <h6><i class="fa fa-envelope mr-2"></i>info@bakanto.com</h6>
                    </li>
                </ul>
            </div>
            <div class="col-lg-6 text-lg-right btn-wrapper">
                <a target="_blank" href="#" class="btn btn-neutral btn-icon-only btn-twitter btn-round btn-lg"
                    data-toggle="tooltip" data-original-title="Follow us">
                    <i class="fa fa-twitter"></i>
                </a>
                <a target="_blank" href="#" class="btn btn-neutral btn-icon-only btn-facebook btn-round btn-lg"
                    data-toggle="tooltip" data-original-title="Like us">
                    <i class="fa fa-facebook-square"></i>
                </a>
            </div>
        </div>
        <hr>
        <div class="row align-items-center justify-content-md-between">
            <div class="col-md-6">
                <div class="copyright">
                    &copy; 2018
                    <a href="#" target="_blank">Bakanto</a>.
                </div>
            </div>
            <div class="col-md-6">
                <ul class="nav nav-footer justify-content-end">
                    <li class="nav-item">
                        <a href="#" class="nav-link" target="_blank">Privacy Policy</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link" target="_blank">Terms Of Service</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link" target="_blank">FAQs</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>