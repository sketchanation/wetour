@extends('frontend.layouts.app')

@section('content')
<main>
        <section class="section section-md section-shaped bakanto-shadow">
        <div class="shape bakanto-shape-home" style="background-image: url('{{$tour->featured_img}}'); background-position: bottom center;"></div>
            <div class="container shape-container d-flex align-items-center py-3">
                <div class="col px-0">
                    <div class="row">
                        <div class="col-lg-6 text-left">
                        <p class="text-white bakanto-text-home">{{$tour->name}}</p>
                            <p class="text-white" style="text-shadow: -1px 2px 1px #0e0e0e;">
                                {{$tour->excerpt}}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                            @if ($tour->status)
                            <div class="form-container px-3 py-3 border">
                                @guest
                            <h4 class="black bold mt-3 px-4 pb-2 text-center">
                            <a href="{{route('login')}}">{{__('Login')}}</a> or <a href="{{route('register')}}">{{__('register')}}</a> {{__('to Book this tour')}}</h4>
                                @endguest
                               

                                @if( auth()->check() and auth()->user()->hasRole('Customer') )
                                @if ( auth()->user()->getReservedTours()->pluck('id')->contains($tour->id)  )
                                    <div class="alert alert-warning">
                                        <p class="text-center">
                                    You have an upcoming reservation for this tour <br> <strong> {{ auth()->user()->getRecentReservation($tour)->carbon_date->diffForHumans() }} </strong> ! 
                                    On the <strong>{{ auth()->user()->getRecentReservation($tour)->getDateString() }}</strong>
                                    in the <strong> {{ auth()->user()->getRecentReservation($tour)->day_time->name }} </strong>
                                    for <strong>{{ auth()->user()->getRecentReservation($tour)->persons }} person(s) </strong> <br>
                                    Check the details <a class="alert-link" href="{{ route('reservation.show',auth()->user()->getRecentReservation($tour)) }}"> here</a>.
                                        </p>
                                    </div>
                                @endif
                                <h4 class="black bold mt-3 px-4 pb-2 text-center">{{__('common.Book this tour')}}</h4>

                            <form id="sidebar-form" action="{{route('tour.book',$tour)}}" method="POST" class="px-xl-3 px-lg-3 px-3"> 
                                    @csrf
                                     <div class="form-group text-center">
                                     <label class="text-center" for="inputtours">{{__('common.Persons attending')}}</label>
                                     <div class="input-group">
                                        <input type="number" name="persons" value="{{ old('persons') }}" class="form-control{{ $errors->has('persons') ? ' is-invalid' : '' }}"  id="inputtours" required>
                                            <div class="input-group-append">
                                                <div class="input-group-text"><i class="fa fa-users"></i> </div>
                                            </div>
                                        </div>
                                    </div>
                                @if ($errors->has('persons'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('persons') }}</strong>
                                    </span>
                                @endif

                                <div class="form-group text-center">
                                <label class="text-center" for="inputtours">{{__('common.When are you attending')}}</label>
                                
                                    <select name="day_time" value="{{ old('day_time') }}" 
                                    class="form-control{{ $errors->has('day_time') ? ' is-invalid' : '' }}"  
                                    id="inputtours" placeholder="Monday" required>
                                    @foreach ($tour->day_times as $day)
                                <option value="{{$day->id}}">{{$day->name}}</option>
                                    @endforeach
                                </select>
                                </div>
                            @if ($errors->has('day_time'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('day_time') }}</strong>
                                </span>
                            @endif

                                    <div class="form-group text-center departure">
                                    <label class="" for="datepicker">{{__('common.Departure Date')}}</label>
                                        <div class="input-group">
                                        <input type="input" name="date" value="{{ old('date') }}" class="form-control datepicker hasDatepicker{{ $errors->has('date') ? ' is-invalid' : '' }}" id="datepicker" placeholder="{{__('common.Choose your Date')}}"  required>
                                            <div class="input-group-append">
                                                <div class="input-group-text"><i class="fa fa-calendar"></i> </div>
                                            </div>
                                        </div>
                                    @if ($errors->has('date'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('date') }}</strong>
                                    </span>
                                @endif
                                    </div>  
                                
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                        <button type="submit" class="btn col-sm-12 my-2 btn-primary">{{__('common.Book Now')}}</button>
                                        </div>
                                    </div>
                                </form>
                                @else
                            <h6 class="black semibold text-center mx-4 mt-3 mb-3 warning-title">Admins cant have tours</h6>
                                @endif
                            </div>
                            
                            @endif

                            <div class="more-info mx-auto my-4">
                            <h6 class="black semibold text-center mx-4 mt-3 mb-3 info-title">{{__('common.Quick Contact')}}</h6>
                                <div class="pb-2">                      
                            
                                    <a href="tel:+133331111"><h5 class="grey text-center tel-info"><i class="fas primary-color fa-phone faa-tada animated mr-2 grey my-lg-0 mb-1"></i>(+1) 3333.1111</h5></a>  
                                    <a href="mailto:hello@ourcompany.com"><h5 class="grey text-center mail-info"><i class="fas fa-envelope faa-horizontal animated primary-color mr-2"></i>hello@ourcompany.com</h5></a>                        
                                </div>
                            </div>
                    </div>
                    <div class="col-md-9">
                        <div class="container">
                            <div class="row justify-content-center text-center">
                                <div class="col-md-12">
                                    <h1 class="display-3">Things to know</h1>
                                    <hr style="width: 30px; height: 4px; background-color: #fb6340; display: block; margin: 15px auto 54px;">
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-lg-4 col-sm-4 col-12 text-left">
                                <h6 class="primary-color semibold price-big">Since {{$tour->price}}€<span class="semibold subtitle">&nbsp;/
                                            Per Person</span> </h6>
                                </div>
                                <div class="col-sm-8 col-12 text-left ml-sm-0">
                                    <div class=" ml-0 mt-1">
                                        @foreach ($tour->tags as $tag)
                                    <span class="badge badge-pill badge-warning">{{$tag->name}}</span>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3 col-6 order-1 order-lg-1">
                                    <img class="svgcenter mb-2 age-icon" src="http://bakanto.com/img/svgs/age.svg"
                                        style="width: 60px;">
                                </div>
                                <div class="col-lg-3 col-6 order-2 order-lg-2">
                                    <img class="svgcenter mb-2 age-icon" src="http://bakanto.com/img/svgs/duration.svg"
                                        style="width: 60px;">
                                </div>
                                <div class="col-lg-3 col-6 order-5 order-lg-3">
                                    <img class="svgcenter mb-2 age-icon" src="http://bakanto.com/img/svgs/location.svg"
                                        style="width: 60px;">
                                </div>
                                <div class="col-lg-3 col-6 order-6 order-lg-4">
                                    <img class="svgcenter mb-2 age-icon" src="http://bakanto.com/img/svgs/calendar.svg"
                                        style="width: 60px;">
                                </div>
                                <div class="col-lg-3 col-6 order-3 order-lg-5">
                                <p class="grey text-center">Age<br><span class="black bold">+{{$tour->min_age}}</span></p>
                                </div>

                                <div class="col-lg-3 col-6 order-4 order-lg-6">
                                    <p class="grey text-center">Duration<br><span class="black bold">{{$tour->length}} Hours</span></p>
                                </div>
                                <div class="col-lg-3 col-6 order-7 order-lg-7">
                                    <p class="grey text-center">Location<br><span class="black bold">{{$tour->location->name}}</span></p>
                                </div>
                                <div class="col-lg-3 col-6 order-8 order-lg-8">
                                    <p class="grey text-center mx-2">Shedule<br><span class="black bold">
                                            @foreach ($tour->active_days as $day)
                                            @unless ( $loop->first)
                                                -
                                            @endunless
                                                {{$day}}
                                            @endforeach
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <ul class="single-tour-container p-0">
                                        <li>
                                            <div class="tour-item-title list-font">Starting Point</div>
                                            <div class="tour-item-description list-font">Plaza de Oriente</div>
                                        </li>
                                        <li>
                                            <div class="tour-item-title list-font">Starting Time</div>
                                            <div class="tour-item-description list-font">1030 am</div>
                                        </li>
                                        <li>
                                            <div class="tour-item-title list-font">Included </div>
                                            <div class="tour-item-description list-font">
                                                    @foreach ($tour->features as $feature)
                                                    <div><i class="fa fa-check-circle badge-primary"></i> {{$feature->name}}</div>
                                                  @endforeach   
                                            </div>
                                        </li>
                                        <li>
                                            <div class="tour-item-title list-font">Not Included</div>
                                            <div class="tour-item-description list-font">
                                                @foreach ($tour->not_features as $feature)
                                          <div><i class="fa fa-times-circle badge-primary"> </i> {{$feature->name}}</div>
                                        @endforeach  
                                            </div>
                                        </li>
                                        <li>
                                            <div class="tour-item-title list-font">Kind Of Tour</div>
                                            <div class="tour-item-description list-font">
                                                @foreach ($tour->group_types as $type)
                                                <div>{{$type->name}}</div>
                                              @endforeach  
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            
                            <div class="row justify-content-center text-center mb-4">
                                <div class="col-md-12">
                                    <h1 class="display-3">Points of interest</h1>
                                    <hr style="width: 30px; height: 4px; background-color: #fb6340; display: block; margin: 15px auto 54px;">
                                    <div id="carousel_example" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                            @foreach ($tour->points as $point)
                                        <li data-target="#carousel_example" data-slide-to="{{$loop->index}}" class="active"></li>
                                            @endforeach
                                    </ol>
                                    <div class="carousel-inner">
                                      @foreach ($tour->points as $point)
                                    <div class="carousel-item  {{$loop->index == 0 ? 'active' : ''}}">
                                    <img src="{{$point->imgStorage[0]}}"
                                                  class="d-block w-100" alt="...">
                                              <div class="carousel-caption d-none d-md-block">
                                              <h5 class="text-white">{{$point->name}}</h5>
                                              <p>{!! $point->description !!}</p>
                                              </div>
                                          </div>
                                      @endforeach
                                    </div>
                                    <a class="carousel-control-prev" href="#carousel_example" role="button" data-slide="prev">
                                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                      <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carousel_example" role="button" data-slide="next">
                                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                      <span class="sr-only">Next</span>
                                    </a>
                                  </div>
                                    <div class="carousel slide">
                                           
                                    </div>
                                    

                                </div>
                            </div>
                            <div class="row justify-content-center text-center">
                                <div class="col-md-12">
                                    <h1 class="display-3">What You'll See</h1>
                                    <hr style="width: 30px; height: 4px; background-color: #fb6340; display: block; margin: 15px auto 54px;">
                                    <div id="map" style="min-height:400px"></div>
                                </div>
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </section>
    </main>
@endsection

@section('scripts')
<script>
    Array.prototype.diff = function(a) {
    return this.filter(function(i) {return a.indexOf(i) < 0;});
};
    var days = []
    var avaliableDays = [0,1,2,3,4,5,6]
      @foreach($tour->active_days as $day)
        days.push( {{ date("w", strtotime($day))  }} )
    @endforeach 
    var disabledDays = avaliableDays.diff( days );
    
$('.datepicker')[0] && $('.datepicker').each(function() {
  
        $('.datepicker').datepicker({
            disableTouchKeyboard: true,
            autoclose: false,
            format: 'yyyy-m-dd',
            daysOfWeekDisabled:  disabledDays,
            startDate: new Date()
        });
    });




    function initMap() {
    // The location of center
    var center = {lat: {{$tour->points->first()->lat}}, lng:  {{$tour->points->first()->lon}} };
    // The map, centered at center
    var map = new google.maps.Map(
        document.getElementById('map'), {zoom: 4, center: center});

    var bounds = new google.maps.LatLngBounds();

    @foreach( $tour->points as $point)
    var contentString{{$loop->index}} = '<div id="content">'+
      '<div id="infowindow">'+
      '<img class="img-fluid" style="max-width:100%" src="{{collect($point->imgStorage)->first()}}" alt="">'+
      '</div>'+
      '<h4 id="firstHeading" class="info-heading">{{$point->name}}</h4>'+
      '<div id="bodyContent" class="info-body">'+
      '{!!$point->description!!}'+
      '</div>';
      var infowindow{{$loop->index}} = new google.maps.InfoWindow({
    content: contentString{{$loop->index}},
    maxWidth: 300
     });
    // The marker, positioned at center
    var marker{{$loop->index}} = new google.maps.Marker({
        position: {lat: {{$point->lat}}, lng:  {{$point->lon}} },
        map: map,
        label: '{{$loop->iteration}}'
        });
    marker{{$loop->index}}.addListener('click', function() {
    infowindow{{$loop->index}}.open(map, marker{{$loop->index}});
    });
    google.maps.event.addListener(map, 'click', function() {
    infowindow{{$loop->index}}.close();
    });

    bounds.extend(marker{{$loop->index}}.getPosition());
        @endforeach
    map.fitBounds(bounds);
  
    }

</script>
<script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAP_KEY')}}&callback=initMap"
async defer></script>
@endsection