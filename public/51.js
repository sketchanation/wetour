webpackJsonp([51],{

/***/ 360:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(0)
/* script */
var __vue_script__ = __webpack_require__(406)
/* template */
var __vue_template__ = __webpack_require__(407)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/views/UserProfile.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-19ecac0f", Component.options)
  } else {
    hotAPI.reload("data-v-19ecac0f", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 406:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  //
});

/***/ }),

/***/ 407:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-container",
    { attrs: { "fill-height": "", fluid: "", "grid-list-xl": "" } },
    [
      _c(
        "v-layout",
        { attrs: { "justify-center": "", wrap: "" } },
        [
          _c(
            "v-flex",
            { attrs: { xs12: "", md8: "" } },
            [
              _c(
                "material-card",
                {
                  attrs: {
                    color: "green",
                    title: "Edit Profile",
                    text: "Complete your profile"
                  }
                },
                [
                  _c(
                    "v-form",
                    [
                      _c(
                        "v-container",
                        { attrs: { "py-0": "" } },
                        [
                          _c(
                            "v-layout",
                            { attrs: { wrap: "" } },
                            [
                              _c(
                                "v-flex",
                                { attrs: { xs12: "", md4: "" } },
                                [
                                  _c("v-text-field", {
                                    attrs: {
                                      label: "Company (disabled)",
                                      disabled: ""
                                    }
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-flex",
                                { attrs: { xs12: "", md4: "" } },
                                [
                                  _c("v-text-field", {
                                    staticClass: "purple-input",
                                    attrs: { label: "User Name" }
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-flex",
                                { attrs: { xs12: "", md4: "" } },
                                [
                                  _c("v-text-field", {
                                    staticClass: "purple-input",
                                    attrs: { label: "Email Address" }
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-flex",
                                { attrs: { xs12: "", md6: "" } },
                                [
                                  _c("v-text-field", {
                                    staticClass: "purple-input",
                                    attrs: { label: "First Name" }
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-flex",
                                { attrs: { xs12: "", md6: "" } },
                                [
                                  _c("v-text-field", {
                                    staticClass: "purple-input",
                                    attrs: { label: "Last Name" }
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-flex",
                                { attrs: { xs12: "", md12: "" } },
                                [
                                  _c("v-text-field", {
                                    staticClass: "purple-input",
                                    attrs: { label: "Adress" }
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-flex",
                                { attrs: { xs12: "", md4: "" } },
                                [
                                  _c("v-text-field", {
                                    staticClass: "purple-input",
                                    attrs: { label: "City" }
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-flex",
                                { attrs: { xs12: "", md4: "" } },
                                [
                                  _c("v-text-field", {
                                    staticClass: "purple-input",
                                    attrs: { label: "Country" }
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-flex",
                                { attrs: { xs12: "", md4: "" } },
                                [
                                  _c("v-text-field", {
                                    staticClass: "purple-input",
                                    attrs: {
                                      label: "Postal Code",
                                      type: "number"
                                    }
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-flex",
                                { attrs: { xs12: "" } },
                                [
                                  _c("v-textarea", {
                                    staticClass: "purple-input",
                                    attrs: {
                                      label: "About Me",
                                      value:
                                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
                                    }
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-flex",
                                { attrs: { xs12: "", "text-xs-right": "" } },
                                [
                                  _c(
                                    "v-btn",
                                    {
                                      staticClass: "mx-0 font-weight-light",
                                      attrs: { color: "success" }
                                    },
                                    [
                                      _vm._v(
                                        "\n                  Update Profile\n                "
                                      )
                                    ]
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-flex",
            { attrs: { xs12: "", md4: "" } },
            [
              _c(
                "material-card",
                { staticClass: "v-card-profile" },
                [
                  _c(
                    "v-avatar",
                    {
                      staticClass: "mx-auto d-block",
                      attrs: { slot: "offset", size: "130" },
                      slot: "offset"
                    },
                    [
                      _c("img", {
                        attrs: {
                          src:
                            "https://demos.creative-tim.com/vue-material-dashboard/img/marc.aba54d65.jpg"
                        }
                      })
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "v-card-text",
                    { staticClass: "text-xs-center" },
                    [
                      _c(
                        "h6",
                        {
                          staticClass:
                            "category text-gray font-weight-thin mb-3"
                        },
                        [_vm._v("CEO / CO-FOUNDER")]
                      ),
                      _vm._v(" "),
                      _c(
                        "h4",
                        { staticClass: "card-title font-weight-light" },
                        [_vm._v("Alec Thompson")]
                      ),
                      _vm._v(" "),
                      _c(
                        "p",
                        { staticClass: "card-description font-weight-light" },
                        [
                          _vm._v(
                            "Don't be scared of the truth because we need to restart the human foundation in truth And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is..."
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "v-btn",
                        {
                          staticClass: "font-weight-light",
                          attrs: { color: "success", round: "" }
                        },
                        [_vm._v("Follow")]
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-19ecac0f", module.exports)
  }
}

/***/ })

});