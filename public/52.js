webpackJsonp([52],{

/***/ 359:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(0)
/* script */
var __vue_script__ = __webpack_require__(404)
/* template */
var __vue_template__ = __webpack_require__(405)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/views/Tours/Index.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-935c2122", Component.options)
  } else {
    hotAPI.reload("data-v-935c2122", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 404:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            headers: [{
                text: 'Name',
                value: 'name'
            }, {
                sortable: false,
                text: 'Location',
                value: 'Location'
            }, {
                sortable: false,
                text: 'Price',
                value: 'Price'
            }, {
                sortable: false,
                text: 'Image',
                value: 'image'
            }, {
                sortable: false,
                text: 'Status',
                value: 'status'
            }, {
                sortable: false,
                text: 'Actions',
                value: 'actions'
            }],
            rowsLength: [5, 15, 25, { text: 'All', value: -1 }],
            loading: false,
            search: null,
            items: []
        };
    },
    created: function created() {
        this.initialize();
    },

    methods: {
        initialize: function initialize() {
            var _this = this;

            this.loading = true;
            axios.get('/tour').then(function (response) {
                _this.loading = false;
                console.log(response);
                _this.items = response.data.data;
            });
        },
        changeStatus: function changeStatus(item) {
            if (!item.status) {
                axios.post('/tour/' + item.alias + '/activate').then(function (response) {
                    item.status = true;
                }).catch(function (err) {
                    console.log(err);
                });
            } else {
                axios.post('/tour/' + item.alias + '/deactivate').then(function (response) {
                    item.status = false;
                }).catch(function (err) {
                    console.log(err);
                });
            }
        },
        editItem: function editItem(item) {
            this.$router.push('/tours/' + item.alias);
        },
        translateItem: function translateItem(item) {
            this.$router.push({
                name: 'tour-translate',
                params: { id: item.alias }
            });
        },
        deleteItem: function deleteItem(item) {
            var _this2 = this;

            var index = this.items.indexOf(item);
            confirm(this.$t('common.confirm_delete', {
                item: this.$t('fields.tour')
            })) && axios.delete('/tour/' + this.items[index].alias).then(function (response) {
                _this2.items.splice(index, 1);
            });
        }
    }
});

/***/ }),

/***/ 405:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-container",
    { attrs: { "fill-height": "", fluid: "", "grid-list-xl": "" } },
    [
      _c(
        "v-layout",
        { attrs: { "justify-center": "", wrap: "" } },
        [
          _c(
            "v-flex",
            { attrs: { md12: "", "justify-end": "" } },
            [
              _c(
                "v-btn",
                {
                  attrs: {
                    dark: "",
                    small: "",
                    to: "tours/create",
                    color: "primary"
                  }
                },
                [
                  _c("v-icon", { attrs: { dark: "" } }, [_vm._v("mdi-plus")]),
                  _vm._v("\n        New\n      ")
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "material-card",
                { attrs: { color: "primary" } },
                [
                  _c(
                    "template",
                    { slot: "header" },
                    [
                      _c(
                        "v-layout",
                        [
                          _c("v-flex", { attrs: { xs6: "" } }, [
                            _c(
                              "h4",
                              { staticClass: "title font-weight-light mb-2" },
                              [_vm._v("Tours")]
                            ),
                            _vm._v(" "),
                            _c(
                              "p",
                              { staticClass: "category font-weight-thin" },
                              [_vm._v("  Tours")]
                            )
                          ]),
                          _vm._v(" "),
                          _c(
                            "v-flex",
                            { attrs: { xs6: "" } },
                            [
                              _c("v-text-field", {
                                staticClass: "white--text",
                                attrs: {
                                  label: _vm.$t("common.search"),
                                  "append-icon": "mdi-search"
                                },
                                model: {
                                  value: _vm.search,
                                  callback: function($$v) {
                                    _vm.search = $$v
                                  },
                                  expression: "search"
                                }
                              })
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-data-table",
                    {
                      attrs: {
                        headers: _vm.headers,
                        items: _vm.items,
                        loading: _vm.loading,
                        search: _vm.search,
                        "rows-per-page-items": _vm.rowsLength,
                        "no-data-text": _vm.$t("common.norecords"),
                        "rows-per-page-text": _vm.$t("common.rows_per_page")
                      },
                      scopedSlots: _vm._u([
                        {
                          key: "headerCell",
                          fn: function(ref) {
                            var header = ref.header
                            return [
                              _c("span", {
                                staticClass:
                                  "subheading font-weight-light text-success text--darken-3",
                                domProps: { textContent: _vm._s(header.text) }
                              })
                            ]
                          }
                        },
                        {
                          key: "items",
                          fn: function(ref) {
                            var item = ref.item
                            return [
                              _c("td", [_vm._v(_vm._s(item.name))]),
                              _vm._v(" "),
                              _c("td", [
                                _vm._v(
                                  _vm._s(item.location.country.name) + " "
                                ),
                                _c("small", [_vm._v(">")]),
                                _vm._v(" " + _vm._s(item.location.name))
                              ]),
                              _vm._v(" "),
                              _c("td", [_vm._v(_vm._s(item.price))]),
                              _vm._v(" "),
                              _c(
                                "td",
                                [
                                  _c("v-avatar", [
                                    _c("img", {
                                      attrs: {
                                        src: item.featured_img,
                                        alt: item.alias
                                      }
                                    })
                                  ])
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "td",
                                [
                                  _c(
                                    "v-chip",
                                    {
                                      staticClass: "text-xs-center",
                                      attrs: {
                                        color:
                                          item.status == 1
                                            ? "green lighten-2"
                                            : "red lighten-2",
                                        "text-color": "white"
                                      }
                                    },
                                    [
                                      _c(
                                        "v-btn",
                                        {
                                          attrs: { icon: "" },
                                          on: {
                                            click: function($event) {
                                              $event.preventDefault()
                                              _vm.changeStatus(item)
                                            }
                                          }
                                        },
                                        [
                                          _c(
                                            "v-icon",
                                            { attrs: { color: "white" } },
                                            [
                                              _vm._v(
                                                "mdi-" +
                                                  _vm._s(
                                                    item.status == 1
                                                      ? "check"
                                                      : "close"
                                                  )
                                              )
                                            ]
                                          )
                                        ],
                                        1
                                      )
                                    ],
                                    1
                                  )
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "td",
                                {},
                                [
                                  _c(
                                    "v-icon",
                                    {
                                      staticClass: "mr-2",
                                      attrs: { small: "" },
                                      on: {
                                        click: function($event) {
                                          _vm.editItem(item)
                                        }
                                      }
                                    },
                                    [
                                      _vm._v(
                                        "\n                mdi-pencil\n              "
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "v-icon",
                                    {
                                      attrs: { small: "" },
                                      on: {
                                        click: function($event) {
                                          _vm.deleteItem(item)
                                        }
                                      }
                                    },
                                    [
                                      _vm._v(
                                        "\n                mdi-delete\n              "
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "a",
                                    {
                                      attrs: {
                                        href: "/tour/" + item.alias,
                                        target: "_blank"
                                      }
                                    },
                                    [
                                      _c(
                                        "v-tooltip",
                                        { attrs: { bottom: "" } },
                                        [
                                          _c(
                                            "v-icon",
                                            {
                                              staticClass: "ml-2",
                                              attrs: {
                                                slot: "activator",
                                                small: ""
                                              },
                                              slot: "activator"
                                            },
                                            [
                                              _vm._v(
                                                "\n                    mdi-arrow-top-right-bold-outline\n                  "
                                              )
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _c("span", [_vm._v("See Tour")])
                                        ],
                                        1
                                      )
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "v-tooltip",
                                    { attrs: { bottom: "" } },
                                    [
                                      _c(
                                        "v-icon",
                                        {
                                          staticClass: "ml-2",
                                          attrs: {
                                            slot: "activator",
                                            small: ""
                                          },
                                          on: {
                                            click: function($event) {
                                              _vm.translateItem(item)
                                            }
                                          },
                                          slot: "activator"
                                        },
                                        [
                                          _vm._v(
                                            "\n                  mdi-book-open-page-variant\n                "
                                          )
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _c("span", [_vm._v("Translate")])
                                    ],
                                    1
                                  )
                                ],
                                1
                              )
                            ]
                          }
                        }
                      ])
                    },
                    [
                      _c("v-progress-linear", {
                        attrs: {
                          slot: "progress",
                          color: "primary",
                          indeterminate: ""
                        },
                        slot: "progress"
                      })
                    ],
                    1
                  )
                ],
                2
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "v-speed-dial",
        { attrs: { fixed: "", bottom: "", right: "", "open-on-hover": "" } },
        [
          _c(
            "v-btn",
            {
              attrs: {
                slot: "activator",
                color: "blue darken-2",
                dark: "",
                fab: "",
                to: "/tours/create"
              },
              slot: "activator"
            },
            [
              _c("v-icon", [_vm._v("mdi-plus")]),
              _vm._v(" "),
              _c("v-icon", [_vm._v("mdi-close")])
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c("router-view")
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-935c2122", module.exports)
  }
}

/***/ })

});