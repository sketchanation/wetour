webpackJsonp([57],{

/***/ 350:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(0)
/* script */
var __vue_script__ = __webpack_require__(376)
/* template */
var __vue_template__ = __webpack_require__(377)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/views/Locations.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-a035eb62", Component.options)
  } else {
    hotAPI.reload("data-v-a035eb62", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 376:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__home_god_Work_pukara_bakanto_node_modules_babel_runtime_core_js_object_assign__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__home_god_Work_pukara_bakanto_node_modules_babel_runtime_core_js_object_assign___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__home_god_Work_pukara_bakanto_node_modules_babel_runtime_core_js_object_assign__);

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            headers: [{
                text: 'Name',
                value: 'name'
            }, {
                sortable: false,
                text: 'Country',
                value: 'Country'
            }, {
                sortable: false,
                text: 'Description',
                value: 'Description'
            }, {
                sortable: false,
                text: 'Created',
                value: 'Created'
            }, {
                sortable: false,
                text: 'Actions',
                value: 'actions'
            }],
            rowsLength: [5, 15, 25, { text: 'All', value: -1 }],
            loading: false,
            form: {},
            formValidate: {
                country: {
                    required: true
                },
                location: {
                    required: true
                }
            },
            countries: [],
            search: null,
            dialog: false,
            filterModel: null,
            populatedCountries: [],
            editedIndex: -1,
            items: [],
            files: []
        };
    },
    computed: {
        formTitle: function formTitle() {
            return this.editedIndex === -1 ? 'New location' : 'Edit location';
        }
    },
    watch: {
        dialog: function dialog(val) {
            val || this.close();
        }
    },
    created: function created() {
        this.initialize();
    },

    methods: {
        initialize: function initialize() {
            var _this = this;

            this.loading = true;
            axios.all([axios.get('/world/countries'), axios.get('/location'), axios.get('/country')]).then(axios.spread(function (countries, location, populatedCountries) {
                _this.items = location.data.data;
                _this.countries = countries.data.data;
                _this.populatedCountries = populatedCountries.data.data;
                _this.loading = false;
            }));
        },
        editItem: function editItem(item) {
            this.editedIndex = this.items.indexOf(item);
            this.form = __WEBPACK_IMPORTED_MODULE_0__home_god_Work_pukara_bakanto_node_modules_babel_runtime_core_js_object_assign___default()({}, item);
            this.files.push({ blob: item.img });
            this.dialog = true;
        },
        save: function save() {
            var _this2 = this;

            this.$validator.validateAll().then(function (validated) {
                if (!validated) return;
                _this2.loading = true;

                if (_this2.files.length) {
                    _this2.form['img'] = _this2.files[0].response ? _this2.files[0].response : _this2.files[0].blob;
                }

                if (_this2.form['description'] == '') {
                    delete _this2.form['description'];
                }
                if (_this2.editedIndex > -1) {
                    var index = _this2.editedIndex;
                    axios.put('/location/' + _this2.form.id, _this2.form).then(function (response) {
                        __WEBPACK_IMPORTED_MODULE_0__home_god_Work_pukara_bakanto_node_modules_babel_runtime_core_js_object_assign___default()(_this2.items[index], response.data.data);
                        _this2.loading = false;
                        _this2.close();
                    });
                } else {
                    axios.post('/location', _this2.form).then(function (response) {
                        _this2.loading = false;
                        _this2.items.push(response.data.data);
                        _this2.close();
                    });
                }
            });
        },
        deleteItem: function deleteItem(item) {
            var _this3 = this;

            var index = this.items.indexOf(item);
            confirm(this.$t('common.confirm_delete', { item: 'location' })) && axios.delete('/location/' + this.items[index].id).then(function (response) {
                _this3.items.splice(index, 1);
            });
        },

        inputFile: function inputFile(newFile, oldFile) {
            if (newFile && oldFile && !newFile.active && oldFile.active) {
                // Get response data
                console.log('response', newFile.response);
                if (newFile.xhr) {
                    //  Get the response status code
                    console.log('status', newFile.xhr.status);
                }
                if (this.files[this.files.length - 1].success) {
                    this.$store.commit('snack', {
                        message: 'Images uploaded Succesfully',
                        type: 'success',
                        icon: 'mdi-check'
                    });
                }
            }
        },
        inputFilter: function inputFilter(newFile, oldFile, prevent) {
            if (newFile && !oldFile) {
                // Filter non-image file
                if (!/\.(jpeg|jpe|jpg|gif|png|webp)$/i.test(newFile.name)) {
                    return prevent();
                }
            }

            // Create a blob field
            newFile.blob = '';
            var URL = window.URL || window.webkitURL;
            if (URL && URL.createObjectURL) {
                newFile.blob = URL.createObjectURL(newFile.file);
            }
        },
        close: function close() {
            var _this4 = this;

            this.$refs.form.reset();
            this.form = {};
            this.files = [];
            this.dialog = false;
            setTimeout(function () {
                _this4.editedIndex = -1;
            }, 300);
        }
    }
});

/***/ }),

/***/ 377:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-container",
    { attrs: { "fill-height": "", fluid: "", "grid-list-xl": "" } },
    [
      _c(
        "v-layout",
        { attrs: { "justify-center": "", wrap: "" } },
        [
          _c(
            "v-flex",
            { attrs: { md12: "", "justify-end": "" } },
            [
              _c(
                "v-btn",
                {
                  attrs: { dark: "", small: "", color: "primary" },
                  on: {
                    click: function($event) {
                      _vm.dialog = !_vm.dialog
                    }
                  }
                },
                [
                  _c("v-icon", { attrs: { dark: "" } }, [_vm._v("mdi-plus")]),
                  _vm._v("\n        New\n      ")
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "material-card",
                { attrs: { color: "primary" } },
                [
                  _c(
                    "template",
                    { slot: "header" },
                    [
                      _c(
                        "v-layout",
                        [
                          _c("v-flex", { attrs: { xs4: "" } }, [
                            _c(
                              "h4",
                              { staticClass: "title font-weight-light mb-2" },
                              [_vm._v("Locations")]
                            ),
                            _vm._v(" "),
                            _c(
                              "p",
                              { staticClass: "category font-weight-thin" },
                              [_vm._v("  Locations")]
                            )
                          ]),
                          _vm._v(" "),
                          _c(
                            "v-flex",
                            { attrs: { xs8: "" } },
                            [
                              _c("v-text-field", {
                                staticClass: "white--text",
                                attrs: {
                                  label: _vm.$t("common.search"),
                                  "append-icon": "mdi-search"
                                },
                                model: {
                                  value: _vm.search,
                                  callback: function($$v) {
                                    _vm.search = $$v
                                  },
                                  expression: "search"
                                }
                              })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-flex",
                            { attrs: { xs4: "" } },
                            [
                              _vm.populatedCountries.length > 1
                                ? _c("v-autocomplete", {
                                    staticClass: "white--text",
                                    attrs: {
                                      items: _vm.populatedCountries,
                                      "item-text": "name",
                                      "item-value": "id",
                                      label: "Filter by country",
                                      clearable: ""
                                    },
                                    model: {
                                      value: _vm.filterModel,
                                      callback: function($$v) {
                                        _vm.filterModel = $$v
                                      },
                                      expression: "filterModel"
                                    }
                                  })
                                : _vm._e()
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-data-table",
                    {
                      attrs: {
                        headers: _vm.headers,
                        items: _vm.items.filter(function(x) {
                          return !_vm.filterModel
                            ? true
                            : x.country_id == _vm.filterModel
                        }),
                        loading: _vm.loading,
                        search: _vm.search,
                        "rows-per-page-items": _vm.rowsLength,
                        "no-data-text": _vm.$t("common.norecords"),
                        "rows-per-page-text": _vm.$t("common.rows_per_page")
                      },
                      scopedSlots: _vm._u([
                        {
                          key: "headerCell",
                          fn: function(ref) {
                            var header = ref.header
                            return [
                              _c("span", {
                                staticClass:
                                  "subheading font-weight-light text-success text--darken-3",
                                domProps: { textContent: _vm._s(header.text) }
                              })
                            ]
                          }
                        },
                        {
                          key: "items",
                          fn: function(ref) {
                            var item = ref.item
                            return [
                              _c("td", [_vm._v(_vm._s(item.name))]),
                              _vm._v(" "),
                              _c("td", [_vm._v(_vm._s(item.country.name))]),
                              _vm._v(" "),
                              _c("td", [_vm._v(_vm._s(item.description))]),
                              _vm._v(" "),
                              _c("td", {}, [_vm._v(_vm._s(item.created_at))]),
                              _vm._v(" "),
                              _c(
                                "td",
                                {},
                                [
                                  _c(
                                    "v-icon",
                                    {
                                      staticClass: "mr-2",
                                      attrs: { small: "" },
                                      on: {
                                        click: function($event) {
                                          _vm.editItem(item)
                                        }
                                      }
                                    },
                                    [
                                      _vm._v(
                                        "\n                mdi-pencil\n              "
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "v-icon",
                                    {
                                      attrs: { small: "" },
                                      on: {
                                        click: function($event) {
                                          _vm.deleteItem(item)
                                        }
                                      }
                                    },
                                    [
                                      _vm._v(
                                        "\n                mdi-delete\n              "
                                      )
                                    ]
                                  )
                                ],
                                1
                              )
                            ]
                          }
                        }
                      ])
                    },
                    [
                      _c("v-progress-linear", {
                        attrs: {
                          slot: "progress",
                          color: "primary",
                          indeterminate: ""
                        },
                        slot: "progress"
                      })
                    ],
                    1
                  )
                ],
                2
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "v-speed-dial",
        { attrs: { fixed: "", bottom: "", right: "", "open-on-hover": "" } },
        [
          _c(
            "v-btn",
            {
              attrs: {
                slot: "activator",
                color: "blue darken-2",
                dark: "",
                fab: ""
              },
              on: {
                click: function($event) {
                  _vm.dialog = !_vm.dialog
                }
              },
              slot: "activator"
            },
            [
              _c("v-icon", [_vm._v("mdi-plus")]),
              _vm._v(" "),
              _c("v-icon", [_vm._v("mdi-close")])
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "v-dialog",
        {
          attrs: { "max-width": "800px" },
          on: {
            keydown: function($event) {
              if (
                !("button" in $event) &&
                _vm._k($event.keyCode, "esc", 27, $event.key, "Escape")
              ) {
                return null
              }
              return _vm.close($event)
            }
          },
          model: {
            value: _vm.dialog,
            callback: function($$v) {
              _vm.dialog = $$v
            },
            expression: "dialog"
          }
        },
        [
          _c(
            "v-card",
            [
              _c("v-card-title", [
                _c("span", { staticClass: "headline" }, [
                  _vm._v(_vm._s(_vm.formTitle))
                ])
              ]),
              _vm._v(" "),
              _c(
                "v-card-text",
                [
                  _c(
                    "v-form",
                    { ref: "form", attrs: { "lazy-validation": "" } },
                    [
                      _c(
                        "v-layout",
                        {
                          attrs: {
                            wrap: "",
                            "justify-center": "",
                            "align-center": ""
                          }
                        },
                        [
                          _c("v-flex", { attrs: { xs10: "" } }, [
                            _c("div", { staticClass: "example-avatar" }, [
                              _c("div", { staticClass: "avatar-upload" }, [
                                _c(
                                  "div",
                                  { staticClass: "text-center p-2" },
                                  [
                                    _c(
                                      "file-upload",
                                      {
                                        ref: "upload",
                                        staticClass: "text-center p-2",
                                        attrs: {
                                          headers: { "X-CSRF-TOKEN": _vm.csrf },
                                          name: "location",
                                          "post-action": "/upload/image"
                                        },
                                        on: {
                                          "input-filter": _vm.inputFilter,
                                          "input-file": _vm.inputFile
                                        },
                                        model: {
                                          value: _vm.files,
                                          callback: function($$v) {
                                            _vm.files = $$v
                                          },
                                          expression: "files"
                                        }
                                      },
                                      [
                                        !_vm.files.length
                                          ? _c("v-img", {
                                              ref: "editImage",
                                              staticClass: "rounded-circle",
                                              staticStyle: { margin: "0 auto" },
                                              attrs: {
                                                src: "/img/404.jpg",
                                                height: "300",
                                                contain: ""
                                              }
                                            })
                                          : [
                                              _c("v-img", {
                                                ref: "editImage",
                                                staticClass: "rounded-circle",
                                                staticStyle: {
                                                  margin: "0 auto"
                                                },
                                                attrs: {
                                                  src: _vm.files[0].blob,
                                                  height: "300",
                                                  contain: ""
                                                }
                                              }),
                                              _vm._v(" "),
                                              _vm.$refs.upload &&
                                              _vm.$refs.upload.active == true
                                                ? _c("v-progress-linear", {
                                                    attrs: {
                                                      value:
                                                        _vm.files[0].progress,
                                                      color: "primary",
                                                      height: "5"
                                                    }
                                                  })
                                                : _vm._e()
                                            ]
                                      ],
                                      2
                                    )
                                  ],
                                  1
                                )
                              ])
                            ])
                          ]),
                          _vm._v(" "),
                          _vm.files.length
                            ? _c(
                                "v-flex",
                                { attrs: { xs2: "" } },
                                [
                                  !_vm.$refs.upload || !_vm.$refs.upload.active
                                    ? _c(
                                        "v-btn",
                                        {
                                          staticClass: "text-xs-center",
                                          attrs: {
                                            slot: "activator",
                                            fab: "",
                                            color: "primary"
                                          },
                                          on: {
                                            click: function($event) {
                                              $event.preventDefault()
                                              _vm.$refs.upload.active = true
                                            }
                                          },
                                          slot: "activator"
                                        },
                                        [
                                          _c(
                                            "v-icon",
                                            { attrs: { small: "", dark: "" } },
                                            [_vm._v("mdi-content-save-outline")]
                                          )
                                        ],
                                        1
                                      )
                                    : _vm._e()
                                ],
                                1
                              )
                            : _vm._e(),
                          _vm._v(" "),
                          _c(
                            "v-flex",
                            { attrs: { xs6: "" } },
                            [
                              _c("v-autocomplete", {
                                directives: [
                                  {
                                    name: "validate",
                                    rawName: "v-validate",
                                    value: _vm.formValidate.country,
                                    expression: "formValidate.country"
                                  }
                                ],
                                attrs: {
                                  items: _vm.countries,
                                  "error-messages": _vm.errors.collect(
                                    _vm.$t("fields.country")
                                  ),
                                  name: _vm.$t("fields.country"),
                                  "no-data-text": _vm.$t("common.nodata"),
                                  label: _vm.$t("fields.country"),
                                  "item-value": "id",
                                  "item-text": "name",
                                  "browser-autocomplete": "disabled",
                                  clearable: "",
                                  chips: ""
                                },
                                model: {
                                  value: _vm.form["country_id"],
                                  callback: function($$v) {
                                    _vm.$set(_vm.form, "country_id", $$v)
                                  },
                                  expression: "form['country_id']"
                                }
                              })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-flex",
                            { attrs: { xs6: "" } },
                            [
                              _c("v-text-field", {
                                directives: [
                                  {
                                    name: "validate",
                                    rawName: "v-validate",
                                    value: _vm.formValidate.location,
                                    expression: "formValidate.location"
                                  }
                                ],
                                attrs: {
                                  "error-messages": _vm.errors.collect(
                                    _vm.$t("fields.location")
                                  ),
                                  name: _vm.$t("fields.location"),
                                  label: _vm.$t("fields.location")
                                },
                                model: {
                                  value: _vm.form["name"],
                                  callback: function($$v) {
                                    _vm.$set(_vm.form, "name", $$v)
                                  },
                                  expression: "form['name']"
                                }
                              })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c("v-textarea", {
                            directives: [
                              {
                                name: "validate",
                                rawName: "v-validate",
                                value: _vm.formValidate.description,
                                expression: "formValidate.description"
                              }
                            ],
                            attrs: {
                              "error-messages": _vm.errors.collect(
                                _vm.$t("fields.description")
                              ),
                              name: _vm.$t("fields.description"),
                              label: _vm.$t("fields.description")
                            },
                            model: {
                              value: _vm.form["description"],
                              callback: function($$v) {
                                _vm.$set(_vm.form, "description", $$v)
                              },
                              expression: "form['description']"
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-card-actions",
                [
                  _c("v-spacer"),
                  _vm._v(" "),
                  _c(
                    "v-btn",
                    {
                      attrs: { color: "error", outline: "" },
                      nativeOn: {
                        click: function($event) {
                          return _vm.close($event)
                        }
                      }
                    },
                    [_vm._v(_vm._s(_vm.$t("common.cancel")))]
                  ),
                  _vm._v(" "),
                  _c(
                    "v-btn",
                    {
                      attrs: {
                        disabled: _vm.errors.any(),
                        color: "primary",
                        outline: ""
                      },
                      nativeOn: {
                        click: function($event) {
                          return _vm.save($event)
                        }
                      }
                    },
                    [_vm._v(_vm._s(_vm.$t("common.save")))]
                  ),
                  _vm._v(" "),
                  _c("v-spacer")
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-a035eb62", module.exports)
  }
}

/***/ })

});