webpackJsonp([54],{

/***/ 355:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(0)
/* script */
var __vue_script__ = __webpack_require__(392)
/* template */
var __vue_template__ = __webpack_require__(393)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/views/Reservations/View.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-3e91d7c4", Component.options)
  } else {
    hotAPI.reload("data-v-3e91d7c4", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 392:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            data: [],
            loading: false
        };
    },
    created: function created() {
        var _this = this;

        this.loading = true;
        axios.get('/reservation/' + this.$route.params.id).then(function (x) {
            _this.data = x.data.data;
            _this.loading = false;
        });
    }
});

/***/ }),

/***/ 393:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-container",
    { attrs: { "fill-height": "", fluid: "", "grid-list-xl": "" } },
    [
      _c(
        "v-layout",
        { attrs: { "justify-center": "", wrap: "" } },
        [
          _c(
            "v-flex",
            { attrs: { xs12: "", md8: "" } },
            [
              _c(
                "material-card",
                {
                  attrs: {
                    text: "Details",
                    color: "accent",
                    title: "Reservation"
                  }
                },
                [
                  _c(
                    "v-layout",
                    {
                      staticClass: "pa-3",
                      attrs: { wrap: "", "justify-center": "" }
                    },
                    [
                      _c("v-flex", { attrs: { xs6: "" } }, [
                        _c("h4", [_vm._v("Bought By")]),
                        _vm._v(" " + _vm._s(_vm.data.user.name))
                      ]),
                      _vm._v(" "),
                      _c("v-flex", { attrs: { xs6: "" } }, [
                        _c("h4", [_vm._v("Email")]),
                        _vm._v(" " + _vm._s(_vm.data.user.email))
                      ]),
                      _vm._v(" "),
                      _c("v-flex", { attrs: { xs6: "" } }, [
                        _c("h4", [_vm._v("Role")]),
                        _vm._v(" " + _vm._s(_vm.data.user.role))
                      ]),
                      _vm._v(" "),
                      _c("v-flex", { attrs: { xs6: "" } }, [
                        _c("h4", [_vm._v("Member since")]),
                        _vm._v(" " + _vm._s(_vm.data.user.created_at))
                      ]),
                      _vm._v(" "),
                      _c("v-flex", { attrs: { xs6: "" } }, [
                        _c("h4", [_vm._v(" Persons attending")]),
                        _vm._v(" " + _vm._s(_vm.data.persons))
                      ]),
                      _vm._v(" "),
                      _c("v-flex", { attrs: { xs6: "" } }, [
                        _c("h4", [_vm._v(" Total Price")]),
                        _vm._v(_vm._s(_vm.data.price))
                      ]),
                      _vm._v(" "),
                      _c("v-flex", { attrs: { xs6: "" } }, [
                        _c("h4", [_vm._v("Payment Method")]),
                        _vm._v(" " + _vm._s(_vm.data.pay_method))
                      ]),
                      _vm._v(" "),
                      _c("v-flex", { attrs: { xs6: "" } }, [
                        _c("h4", [_vm._v(" Transaction Id")]),
                        _vm._v(" " + _vm._s(_vm.data.stripe_transaction_id))
                      ]),
                      _vm._v(" "),
                      _c("v-flex", { attrs: { xs6: "" } }, [
                        _c("h4", [_vm._v(" Reserved for")]),
                        _vm._v(" " + _vm._s(_vm.data.date))
                      ]),
                      _vm._v(" "),
                      _c("v-flex", { attrs: { xs6: "" } }, [
                        _c("h4", [_vm._v(" Reserved Created")]),
                        _vm._v(_vm._s(_vm.data.created_at))
                      ]),
                      _vm._v(" "),
                      _c("v-flex", { attrs: { xs6: "" } }, [
                        _c("h4", [_vm._v(" Tour ")]),
                        _vm._v(_vm._s(_vm.data.tour.name))
                      ])
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-flex",
            { attrs: { xs12: "", md4: "", "justify-center": "" } },
            [
              _c(
                "material-card",
                { staticClass: "v-card-profile" },
                [
                  _c("img", {
                    staticClass: "text-xs-center",
                    attrs: {
                      src: _vm.data.tour.featured_img,
                      contain: "",
                      height: "200"
                    }
                  }),
                  _vm._v(" "),
                  _c("v-card-text", { staticClass: "text-xs-center" }, [
                    _c(
                      "h6",
                      {
                        staticClass: "category text-gray font-weight-thin mb-3"
                      },
                      [
                        _c("v-icon", [_vm._v("mdi-star")]),
                        _vm._v(" " + _vm._s(_vm.data.tour.rate))
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c("h4", { staticClass: "card-title font-weight-light" }, [
                      _vm._v(_vm._s(_vm.data.tour.name))
                    ]),
                    _vm._v(" "),
                    _c(
                      "p",
                      { staticClass: "card-description font-weight-light" },
                      [_vm._v(_vm._s(_vm.data.tour.excerpt))]
                    )
                  ])
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-flex",
            { attrs: { xs12: "" } },
            [
              _c(
                "v-btn",
                {
                  attrs: { color: "primary" },
                  on: {
                    click: function($event) {
                      _vm.$router.back()
                    }
                  }
                },
                [_vm._v("Back")]
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-3e91d7c4", module.exports)
  }
}

/***/ })

});