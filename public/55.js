webpackJsonp([55],{

/***/ 354:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(0)
/* script */
var __vue_script__ = __webpack_require__(390)
/* template */
var __vue_template__ = __webpack_require__(391)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/views/Reservations/Index.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-666c4d29", Component.options)
  } else {
    hotAPI.reload("data-v-666c4d29", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 390:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__home_god_Work_pukara_bakanto_node_modules_babel_runtime_core_js_object_assign__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__home_god_Work_pukara_bakanto_node_modules_babel_runtime_core_js_object_assign___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__home_god_Work_pukara_bakanto_node_modules_babel_runtime_core_js_object_assign__);

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            headers: [{
                text: 'Reserved By',
                value: 'user.name'
            }, {
                text: 'Day Time',
                value: 'day_time'
            }, {
                text: 'Tour',
                value: 'tour.name'
            }, {
                sortable: false,
                text: 'Additional Persons',
                value: 'persons',
                align: "'center'"
            }, {
                text: 'Due to',
                value: 'date'
            }, {
                sortable: false,
                text: 'Guide',
                value: 'Guide'
            }, {
                sortable: false,
                text: 'Actions',
                value: 'actions'
            }],
            rowsLength: [5, 15, 25, { text: 'All', value: -1 }],
            loading: false,
            form: {},
            formValidate: {},
            dialog: false,
            search: null,
            editedIndex: -1,
            items: [],
            guides: []
        };
    },
    created: function created() {
        this.initialize();
    },

    methods: {
        initialize: function initialize() {
            var _this = this;

            this.loading = true;
            axios.all([axios.get('/reservation'), axios.get('/guide')]).then(axios.spread(function (reservation, guides) {
                _this.items = reservation.data.data;
                _this.guides = guides.data.data;
                _this.loading = false;
            }));
        },
        selectGuide: function selectGuide(item) {
            this.form = __WEBPACK_IMPORTED_MODULE_0__home_god_Work_pukara_bakanto_node_modules_babel_runtime_core_js_object_assign___default()({}, item);
            if (item.guide) {
                this.$set(this.form, 'guide_id', item.guide.id);
            }
            this.dialog = true;
        },
        save: function save() {
            var _this2 = this;

            this.loading = true;
            // if (this.item.guide) {
            var index = this.items.map(function (el) {
                return el.id;
            }).indexOf(this.form.id);

            axios.put('/reservation/' + this.form.stripe_transaction_id, this.form).then(function (response) {
                __WEBPACK_IMPORTED_MODULE_0__home_god_Work_pukara_bakanto_node_modules_babel_runtime_core_js_object_assign___default()(_this2.items[index], response.data.data);
                _this2.loading = false;
                _this2.dialog = false;
            });
            // } else {
            //   axios.post("/point", this.form).then(response => {
            //     this.loading = false;
            //     this.items.push(response.data.data);
            //     this.close();
            //   });
            // }
        },
        verItem: function verItem(item) {
            this.$router.push({
                path: '/reservations/' + item.stripe_transaction_id
            });
        },
        goReservations: function goReservations(item) {
            console.log(item);
            this.$router.push({
                path: '/reservations/group',
                query: {
                    date: item.date,
                    day_time: item.day_time.id,
                    tour: item.tour.id
                }
            });
            // window.location.href = window.location.origin + `/reservations/group?date=${item.date}&day_time=${item.day_time}`
        },
        close: function close() {
            var _this3 = this;

            this.$refs.form.reset();
            this.form = {};
            this.dialog = false;
            setTimeout(function () {
                _this3.editedIndex = -1;
            }, 300);
        }
    }
});

/***/ }),

/***/ 391:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-container",
    { attrs: { "fill-height": "", fluid: "", "grid-list-xl": "" } },
    [
      _c(
        "v-layout",
        { attrs: { "justify-center": "", wrap: "" } },
        [
          _c(
            "v-flex",
            { attrs: { md12: "", "justify-end": "" } },
            [
              _c(
                "material-card",
                { attrs: { color: "primary" } },
                [
                  _c(
                    "template",
                    { slot: "header" },
                    [
                      _c(
                        "v-layout",
                        [
                          _c("v-flex", { attrs: { xs6: "" } }, [
                            _c(
                              "h4",
                              { staticClass: "title font-weight-light mb-2" },
                              [_vm._v("Reservations")]
                            ),
                            _vm._v(" "),
                            _c(
                              "p",
                              { staticClass: "category font-weight-thin" },
                              [_vm._v("  reservations")]
                            )
                          ]),
                          _vm._v(" "),
                          _c(
                            "v-flex",
                            { attrs: { xs6: "" } },
                            [
                              _c("v-text-field", {
                                staticClass: "white--text",
                                attrs: {
                                  label: _vm.$t("common.search"),
                                  "append-icon": "mdi-search"
                                },
                                model: {
                                  value: _vm.search,
                                  callback: function($$v) {
                                    _vm.search = $$v
                                  },
                                  expression: "search"
                                }
                              })
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-data-table",
                    {
                      attrs: {
                        headers: _vm.headers,
                        items: _vm.items,
                        loading: _vm.loading,
                        search: _vm.search,
                        "rows-per-page-items": _vm.rowsLength,
                        "no-data-text": _vm.$t("common.norecords"),
                        "rows-per-page-text": _vm.$t("common.rows_per_page")
                      },
                      scopedSlots: _vm._u([
                        {
                          key: "headerCell",
                          fn: function(ref) {
                            var header = ref.header
                            return [
                              _c("span", {
                                staticClass:
                                  "subheading font-weight-light text-success text--darken-3",
                                domProps: { textContent: _vm._s(header.text) }
                              })
                            ]
                          }
                        },
                        {
                          key: "items",
                          fn: function(ref) {
                            var item = ref.item
                            return [
                              _c("td", [_vm._v(_vm._s(item.user.name) + " ")]),
                              _vm._v(" "),
                              _c("td", [_vm._v(_vm._s(item.day_time.name))]),
                              _vm._v(" "),
                              _c("td", [_vm._v(_vm._s(item.tour.name))]),
                              _vm._v(" "),
                              _c("td", { staticClass: "text-center" }, [
                                _vm._v(_vm._s(item.persons))
                              ]),
                              _vm._v(" "),
                              _c("td", {}, [_vm._v(_vm._s(item.date))]),
                              _vm._v(" "),
                              item.guide
                                ? _c("td", {}, [
                                    _vm._v(_vm._s(item.guide.user.name))
                                  ])
                                : _c("td"),
                              _vm._v(" "),
                              _c(
                                "td",
                                {},
                                [
                                  _c(
                                    "v-icon",
                                    {
                                      staticClass: "mr-2",
                                      attrs: { small: "" },
                                      on: {
                                        click: function($event) {
                                          _vm.verItem(item)
                                        }
                                      }
                                    },
                                    [
                                      _vm._v(
                                        "\n                mdi-arrow-right-bold-outline\n              "
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "v-icon",
                                    {
                                      staticClass: "ml-2",
                                      attrs: { small: "" },
                                      on: {
                                        click: function($event) {
                                          _vm.goReservations(item)
                                        }
                                      }
                                    },
                                    [
                                      _vm._v(
                                        "\n                mdi-account-box-multiple\n              "
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  item.full
                                    ? _c(
                                        "v-tooltip",
                                        { attrs: { bottom: "" } },
                                        [
                                          _c(
                                            "v-icon",
                                            {
                                              staticClass: "ml-2",
                                              attrs: {
                                                slot: "activator",
                                                color: "primary"
                                              },
                                              on: {
                                                click: function($event) {
                                                  _vm.selectGuide(item)
                                                }
                                              },
                                              slot: "activator"
                                            },
                                            [
                                              _vm._v(
                                                "\n                  mdi-account-settings\n                "
                                              )
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _c("span", [
                                            _vm._v("Ready for a guide!")
                                          ])
                                        ],
                                        1
                                      )
                                    : _vm._e()
                                ],
                                1
                              )
                            ]
                          }
                        }
                      ])
                    },
                    [
                      _c("v-progress-linear", {
                        attrs: {
                          slot: "progress",
                          color: "primary",
                          indeterminate: ""
                        },
                        slot: "progress"
                      })
                    ],
                    1
                  )
                ],
                2
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "v-dialog",
        {
          attrs: { "max-width": "500px" },
          on: {
            keydown: function($event) {
              if (
                !("button" in $event) &&
                _vm._k($event.keyCode, "esc", 27, $event.key, "Escape")
              ) {
                return null
              }
              return _vm.close($event)
            }
          },
          model: {
            value: _vm.dialog,
            callback: function($$v) {
              _vm.dialog = $$v
            },
            expression: "dialog"
          }
        },
        [
          _c(
            "v-card",
            [
              _c("v-card-title", [
                _c("span", { staticClass: "headline" }, [
                  _vm._v("Assign a guide")
                ])
              ]),
              _vm._v(" "),
              _c(
                "v-card-text",
                [
                  _c(
                    "v-form",
                    { ref: "form", attrs: { "lazy-validation": "" } },
                    [
                      _c(
                        "v-layout",
                        {
                          attrs: {
                            wrap: "",
                            "justify-center": "",
                            "align-center": ""
                          }
                        },
                        [
                          _c(
                            "v-flex",
                            [
                              _c("v-select", {
                                directives: [
                                  {
                                    name: "validate",
                                    rawName: "v-validate",
                                    value: _vm.formValidate.guide,
                                    expression: "formValidate.guide"
                                  }
                                ],
                                attrs: {
                                  items: _vm.guides,
                                  "error-messages": _vm.errors.collect(
                                    _vm.$t("fields.guide")
                                  ),
                                  name: _vm.$t("fields.guide"),
                                  "no-data-text": _vm.$t("common.nodata"),
                                  label: _vm.$t("fields.guide"),
                                  "item-text": "user.name",
                                  "item-value": "id",
                                  "item-avatar": "img",
                                  "browser-autocomplete": "disabled",
                                  clearable: ""
                                },
                                scopedSlots: _vm._u([
                                  {
                                    key: "selection",
                                    fn: function(data) {
                                      return [
                                        _c(
                                          "v-flex",
                                          { attrs: { xs2: "" } },
                                          [
                                            _c(
                                              "v-avatar",
                                              { attrs: { size: "25px" } },
                                              [
                                                _c("img", {
                                                  attrs: { src: data.item.img }
                                                })
                                              ]
                                            )
                                          ],
                                          1
                                        ),
                                        _vm._v(" "),
                                        _c("v-flex", { staticClass: "ml-1" }, [
                                          _vm._v(
                                            "\n                    " +
                                              _vm._s(data.item.user.name) +
                                              "\n                  "
                                          )
                                        ])
                                      ]
                                    }
                                  },
                                  {
                                    key: "item",
                                    fn: function(data) {
                                      return [
                                        _c("v-list-tile-avatar", [
                                          _c("img", {
                                            attrs: { src: data.item.img }
                                          })
                                        ]),
                                        _vm._v(" "),
                                        _c(
                                          "v-list-tile-content",
                                          [
                                            _c("v-list-tile-title", {
                                              domProps: {
                                                innerHTML: _vm._s(
                                                  data.item.user.name
                                                )
                                              }
                                            })
                                          ],
                                          1
                                        )
                                      ]
                                    }
                                  }
                                ]),
                                model: {
                                  value: _vm.form["guide_id"],
                                  callback: function($$v) {
                                    _vm.$set(_vm.form, "guide_id", $$v)
                                  },
                                  expression: "form['guide_id']"
                                }
                              })
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-card-actions",
                [
                  _c("v-spacer"),
                  _vm._v(" "),
                  _c(
                    "v-btn",
                    {
                      attrs: { color: "error", outline: "" },
                      nativeOn: {
                        click: function($event) {
                          return _vm.close($event)
                        }
                      }
                    },
                    [_vm._v(_vm._s(_vm.$t("common.cancel")))]
                  ),
                  _vm._v(" "),
                  _c(
                    "v-btn",
                    {
                      attrs: {
                        disabled: _vm.errors.any(),
                        color: "primary",
                        outline: ""
                      },
                      nativeOn: {
                        click: function($event) {
                          return _vm.save($event)
                        }
                      }
                    },
                    [_vm._v(_vm._s(_vm.$t("common.save")))]
                  ),
                  _vm._v(" "),
                  _c("v-spacer")
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-666c4d29", module.exports)
  }
}

/***/ })

});