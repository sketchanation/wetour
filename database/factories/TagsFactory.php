<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Tag::class, function (Faker $faker) {
    return [
        'name' => $name = $faker->unique()->word,
        'description' => $faker->sentence,
    ];
});
