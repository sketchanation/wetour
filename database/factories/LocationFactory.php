<?php

use Faker\Generator as Faker;
use Khsing\World\World;

$factory->define(App\Models\Location::class, function (Faker $faker) {
    return [
        'name' => $faker->city,
        'description' => $faker->sentence(10,rand(0,50)),
         'country_id' => function(){
            $world = World::Countries()->random();
            $country = App\Models\Country::firstOrCreate([
                'id' => $world->id,
                'name' => $world->name,
                'code' => $world->code,
            ]);
         
            return $world->id;
         }
    ];
});
