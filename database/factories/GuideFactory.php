<?php

use Faker\Generator as Faker;

$factory->define(App\Models\UserGuide::class, function (Faker $faker) {
    return [
        // 'name' => $faker->name,
        // 'email' => $faker->unique()->email,
        'bio' => $faker->sentence(10,rand(0,50)),
        'rate' => rand(1,5),
        'identification' => $faker->creditCardNumber(),
        'phone' => $faker->phoneNumber,
        'user_id' => null,
        'location_id' => null
    ];
});
