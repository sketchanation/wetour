<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Tour::class, function (Faker $faker) {
    return [
        'name' => $name = $faker->sentence,
        'alias' => str_slug($name),
        'max_age' => rand(20,40),
        'min_age' => rand(1,20),
        'price' => rand(1,1000),
        'length' => rand(1,5),
        'status' => $faker->boolean(),
        // 'featured_img' => $faker->imageUrl(),
        'description' => "<div><p>{$faker->sentence(10,rand(0,50))}</p></div>",
        'excerpt' => $faker->sentence(10,rand(0,50)),
        // 'day_time' => [['Morning','Evening','Afternoon','Night'][rand(0,3)]],
        // 'capacity' => 1,
        'active_days' => [['Monday','Thursday','Friday','Sunday','Saturday'][rand(0,4)]],
    ];
});
