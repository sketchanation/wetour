<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Point::class, function (Faker $faker) {
    return [
        'name' => $faker->city,
        'description' => "<div><p>{$faker->sentence(10,rand(0,50))}</p></div>",
        'lat' => $faker->latitude,
        'lon' => $faker->longitude,
        'address' => $faker->address,
        // 'country_id' => App\Models\Country::get()->random()->id,
        'location_id' => App\Models\Location::get()->random()->id,
    ];
});
