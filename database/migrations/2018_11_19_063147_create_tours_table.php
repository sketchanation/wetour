<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateToursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tours', function (Blueprint $table) {
            $table->increments('id');
            $table->json('name');
            $table->json('alias');
            $table->boolean('type')->default(0); // Privado o publico
            $table->string('featured_img')->nullable();
            $table->double('price');
            $table->json('description');
            $table->json('excerpt');
            // $table->enum('includes'); // relacion
            $table->integer('max_age');
            $table->integer('min_age');
            // $table->string('day_time');
            $table->string('active_days');
            // $table->string('start_time');
            $table->float('length');
            // $table->integer('capacity');
            //tags rates
            $table->boolean('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tours');
    }
}
