<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeatureTourTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feature_tour', function (Blueprint $table) {
            $table->increments('id');

            $table->boolean('status');
            
            $table->unsignedInteger('feature_id');
            $table->unsignedInteger('tour_id');

            $table->foreign('feature_id')
            ->references('id')
            ->on('features')
            ->onDelete('cascade');

            $table->foreign('tour_id')
                ->references('id')
                ->on('tours')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feature_tour');
    }
}
