<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('days', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('batch')->default(0); 
            // $table->string('day_time');
            $table->date('date');
            $table->integer('capacity');
            $table->boolean('full')->default(0);

            $table->unsignedInteger('tour_id')->nullable();
            $table->foreign('tour_id')
                ->references('id')
                ->on('tours')
                ->onDelete('cascade');

            $table->unsignedInteger('user_guide_id')->nullable();
            $table->foreign('user_guide_id')
                ->references('id')
                ->on('user_guides')
                ->onDelete('cascade');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('days');
    }
}
