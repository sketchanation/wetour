<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePointTourTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('point_tour', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('order');

            $table->unsignedInteger('point_id');
            $table->foreign('point_id')
                    ->references('id')
                    ->on('points')
                    ->onDelete('cascade');

            $table->unsignedInteger('tour_id');
            $table->foreign('tour_id')
                    ->references('id')
                    ->on('tours')
                    ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('point_tour');
    }
}
