<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            GroupTypesSeeder::class, 
            DayTimesSeeder::class, 
            TagsSeeder::class, 
            FeaturesSeeder::class, 
            WorldTablesSeeder::class 
            ]);

        factory(\App\Models\Tour::class, 30)->create()->each(function ($tour) {
            $tour->group_types()->attach(rand(1, 4));
            $tour->tags()->attach(
                factory(App\Models\Tag::class, 3)->create()
            );
            $tour->day_times()->attach(rand(1, 4));
            $location = factory(App\Models\Location::class)->create();
            $tour->location()->associate($location);

            $tour->points()->attach(
                factory(App\Models\Point::class,2)->create([
                    'location_id' => $location->id,
                    // 'country_id' => $location->country->id,
                    ]),
                    [ 'order' => rand(0,2)]
                );

            $tour->features()->attach(
                factory(App\Models\Feature::class, 3)->create(), ['status' => rand(0, 1)]
            );
            $tour->save();
        });

        $this->call(UsersAndPermissionsSeeder::class); // create users

    }
}
