<?php

use App\Models\GroupType;
use Illuminate\Database\Seeder;

class GroupTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = ['Individual', 'Couples', 'Friends', 'Family'];
        $translations = ['Individual','Parejas','Amigos','Familia'];
        $translationsCHn = ['Wong Wa','Aniki','Sukin','Jaloo'];

        
        for ($i=0; $i < count($types); $i++) { 
            $var =  GroupType::create([
                'name' => $types[$i],
            ]);

            $var->setTranslation('name', 'es', $translations[$i]);
            $var->setTranslation('name', 'zh', $translationsCHn[$i]);
            $var->save();
        }

    }
}
