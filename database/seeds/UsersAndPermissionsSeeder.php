<?php

use App\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class UsersAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $roles = [
            'Admin',
            'Guide',
            'Customer',
        ];

        foreach ($roles as $role) {
            // Crear Roles
            Role::create(['name' => $role]);

        }

        $user = User::create([
            'name' => env('BAKANTO_ADMIN_USER_NAME'),
            'email' => env('BAKANTO_ADMIN_USER_EMAIL'),
            'password' => Hash::make(env('BAKANTO_ADMIN_USER_PASSWORD')),
        ]);

        // Asigna el Rol Administrador para el usuario por defecto
        $user->assignRole('Admin');

        $guides = factory(App\User::class, 5)->create()->each(function ($u) {
            $guide = factory(App\Models\UserGuide::class)->create([
                'location_id' => App\Models\Location::all()->random()->id,
            ]);
            $u->assignRole('Guide');
            $u->user_guide_id= $guide->id;
            $guide->user()->associate($u);
            $guide->save();
            $u->save();
        });
        $tags = App\Models\Tag::all()->pluck('id');
        $customers = factory(App\User::class, 5)->create()->each(function ($u) use ($tags) {
            $u->assignRole('Customer');
            $u->tags()->attach($tags->random(3)->all());
            $u->save();
        });

    }
}
