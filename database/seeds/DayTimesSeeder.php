<?php

use Illuminate\Database\Seeder;

use App\Models\DayTime;

class DayTimesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $times = ['Afternoon', 'Noon', 'Morning', 'Night'];
        $translations = ['Tarde','MedioDia','Manana','Noche'];
        $translationsCHn = ['Wong Wa','Aniki','Sukin','Jaloo'];

        
        for ($i=0; $i < count($times); $i++) { 
            $var =  DayTime::create([
                'name' => $times[$i],
            ]);

            $var->setTranslation('name', 'es', $translations[$i]);
            $var->setTranslation('name', 'zh', $translationsCHn[$i]);
            $var->save();
        }
    }
}
