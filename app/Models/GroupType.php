<?php

namespace App\Models;

use App\Models\Tour;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class GroupType extends Model
{
    use HasTranslations;
    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var array
     * The attributes translatable
     */
    protected $translatable = ['name', 'description'];

    /**
     * The tour with this group type
     * @return App\Models\Tour
     */
    public function tours()
    {
        return $this->belongsToMany(Tour::class);
    }
}
