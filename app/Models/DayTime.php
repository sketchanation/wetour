<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\{Reservation, Day, Tour};
use Spatie\Translatable\HasTranslations;

class DayTime extends Model
{
    use HasTranslations;
    
    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var array
     * The attributes translatable
     */
    protected $translatable = ['name', 'description'];


    /**
     * The tours that include this day_time
     * @return App\Models\Tour
     */
    public function tours()
    {
        return $this->belongsToMany(Tour::class);
    }

    /**
     * The reservations that include this day_time 
     * @return \App\Models\Reservation
     */
    public function reservations()
    {
        return $this->hasMany(Reservation::class);
    }

    /**
     * The days that include this day_time
     * @return \App\Models\Day
     */
    public function days()
    {
        return $this->hasMany(Day::class);
    }
}
