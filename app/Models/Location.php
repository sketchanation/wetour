<?php

namespace App\Models;

use App\Models\{Country, Tour, UserGuide as Guide, Point};
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * The relationships to be loaded with this model
     */
    protected $with = [
        'country',
    ];

    /**
     * The country this city belongs to
     * @return App\Models\Country
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    /**
     * The tours in this location
     * @return App\Models\Tour
     */
    public function tours()
    {
        return $this->hasMany(Tour::class);
    }

    /**
     * The tours in this location
     * @return App\Models\Guide
     */
    public function guides()
    {
        return $this->hasMany(Guide::class);
    }

    /**
     * The tours in this location
     * @return App\Models\Point
     */
    public function points()
    {
        return $this->hasMany(Point::class);
    }

    /**
     * Verifies if this location has tours
     * @return bool
     */
    public function hasTours(): bool
    {
        return !$this->tours->isEmpty();
    }

    /**
     * Scope a query to only include the locations with tours.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOccupied($query)
    {
        return $query->whereHas('tours')->get();
    }
    
    public function getImgAttribute($val)
    {
        if (is_null($val)) {
            return '/img/404.jpg';
        }

        return '/storage/' . $val;
    }
}
