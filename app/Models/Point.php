<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\{Tour, Location};

class Point extends Model
{
    /**
     * @var array
     */
    protected $guarded = [];
    
    /**
     * @var array
     */
    protected $casts = [
        'img' => 'array',
    ];

    /**
     * The tours that use this point
     * @return App\Models\Tour
     */
    public function tours()
    {
        return $this->belongsToMany(Tour::class)
                    ->withPivot('order');
    }

    /**
     * The location this tour belongs to
     * @return App\Models\Location
     */
    public function location()
    {
        return $this->belongsTo(Location::class);
    }

    /**
     * Helper method to get the images with the correct route
     */
    public function getImgStorageAttribute()
    {
        if (is_null($this->img)) {
            return collect('/img/404.jpg');
        }

        $result = collect();
        foreach ($this->img as $img) {
            $img =  '/storage/' . $img;
            $result->push($img);
        }
        return $result;
    }
}
