<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

use App\Models\Tour;

class Feature extends Model
{
    use HasTranslations;

    /**
     * @var array
     */
    protected $guarded = [];
    
    /**
     * @var array
     * The attributes translatable
     */
    protected $translatable = ['name', 'description'];

    /**
     * The tours that include this feature
     * @return App\Models\Tour
     */
    public function tours()
    {
        return $this->belongsToMany(Tour::class);
    }
}
