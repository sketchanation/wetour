<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\Reservation;

class Extra extends Model
{
    protected $guarded = [];

    /**
     * The reservation this extra belongs to
     * @return \App\Model\Reservation
     */
    public function reservation()
    {
        return $this->belongsTo(Reservation::class);
    }
}
