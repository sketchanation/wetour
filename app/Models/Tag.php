<?php

namespace App\Models;

use App\Models\Tour;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Tag extends Model
{
    use HasTranslations;
    
    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var array
     * The attributes translatable
     */
    protected $translatable = ['name', 'description'];

    /**
     * The tours with this tag
     * @return App\Models\Tour
     */
    public function tours()
    {
        return $this->morphedByMany(Tour::class, 'taggable');
    }

    /**
     * Get all of the users that are assigned this tag.
     * @return \App\User
     */
    public function users()
    {
        return $this->morphedByMany(User::class, 'taggable');
    }


}
