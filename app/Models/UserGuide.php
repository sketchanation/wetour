<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\User;
use App\Models\{Location, Day};

class UserGuide extends Model
{
    /**
     * @var array
     */
    protected $guarded = [];

    protected $with = [
        'location',
        'user'
    ];

    /**
     * Get the featured image with the correct path
     *
     * @return string
     */
    public function getImgAttribute($val)
    {
        if( is_null($val) ) return '/img/chad.jpeg';
        return '/storage/' . $val;
    }

    /**
     * Get the user guide information
     * @return \App\User
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

   /**
     * The location this tour belongs to
     * @return App\Models\Location
     */
    public function location()
    {
        return $this->belongsTo(Location::class, 'location_id', 'id');
    }

    /**
     * Get the user guide information
     * @return \App\Models\Day
     */
    public function days()
    {
        return $this->hasMany(Day::class);
    }

    /**
     * Check if this user upcoming active days
     * @return bool true if the user guide CAN be deleted
     */
    public function checkDays()
    {
        if( ! $this->days->isEmpty() ){
            foreach( $this->days as $day){
                if ( !$day->isDeleteable() ){
                    return false;
                }
            }
        }
        return true;
    }
}
