<?php

namespace App\Models;

use App\Models\Tour;
use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * The tour where this rating belongs to
     * @return App\Models\Tour
     */
    public function tour()
    {
        return $this->belongsTo(Tour::class);
    }
}
