<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\{Location, Tour};

class Country extends Model
{
    /*
     * @var array
     */
    protected $guarded = [];

    /*
     * Alias for the Country-location relationship
     * The locations in this country
     * @return App\Models\Location
     */
    public function locations()
    {
        return $this->hasMany(Location::class);
    }

    /**
     * The tours in this country
     * @return App\Models\Tour
     */
    public function tours()
    {
        return $this->hasManyThrough(Tour::class, Location::class);
    }
}