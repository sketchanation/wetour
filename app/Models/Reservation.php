<?php

namespace App\Models;

use Illuminate\Database\Eloquent\{Model, SoftDeletes};

use App\Helpers\Traits\CarbonDate;
use App\Models\{Day, Tour, Extra, UserGuide as Guide, DayTime};
use App\User;
class Reservation extends Model
{
    use SoftDeletes, CarbonDate;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * @var array
     * Always preload this relationshops
     */
    protected $with = ['day', 'user','day_time'];

    /**
     * @var array
     */
    protected $guarded = [];
    
    /**
     * The client of this reservation
     * @return \App\User
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * The reservation of this reservation
     * @return \App\Models\Day
     */
    public function day()
    {
        return $this->belongsTo(Day::class);
    }

    /**
     * Get this reservation tour
     * @return \App\Models\Tour
     */
    public function tour()
    {
        return $this->belongsTo(Tour::class);
    }

    /**
     * Get this reservation extras
     * @return \App\Models\Extra
     */
    public function extras()
    {
        return $this->hasMany(Extra::class);
    }

    /**
     * Get this reservation day_time
     * @return \App\Models\DayTime
     */
    public function day_time()
    {
        return $this->belongsTo(DayTime::class);
    }

    /**
     * Get the route key for the model
     * @return bool
     */
    public function getRouteKey()
    {
        return $this->stripe_transaction_id;
    }

}
