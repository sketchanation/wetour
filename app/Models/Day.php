<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Helpers\Traits\CarbonDate;
use App\Models\{Reservation, Tour, UserGuide as Guide, DayTime};

class Day extends Model
{

    use CarbonDate;

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * This day reservations
     * @return \App\Models\Reservation
     */
    public function reservations()
    {
        return $this->hasMany(Reservation::class);
    }

    /**
     * Get this day tour
     * @return \App\Models\Tour
     */
    public function tour()
    {
        return $this->belongsTo(Tour::class);
    }

    /**
     * Get this reservation(day) day_time
     * @return \App\Models\DayTime
     */
    public function day_time()
    {
        return $this->belongsTo(DayTime::class);
    }

    /**
     * Get this day guide
     * @return \App\Models\Guide
     */
    public function guide()
    {
        return $this->belongsTo(Guide::class, 'user_guide_id', 'id');
    }

    /**
     * Check if this day is deleteable
     * @return bool
     */
    public function isDeleteable()
    {
        return ( $this->hasPassed() or $this->reservations->isEmpty() );
    }
}
