<?php

namespace App\Models;

use App\Models\{Location, Feature, GroupType, Rating, Tag, Reservation, Day, Point, DayTime};
use App\User;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Tour extends Model
{
    use HasTranslations;
    
    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var array
     * The attributes translatable
     */
    public $translatable = ['name','alias','description','excerpt'];

    /**
     * The castings needed for this model
     * @var array
     */
    protected $casts = [
        'active_days' => 'array',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['not_features', 'features'];

    /**
     * The ratings of this tour
     * @return App\Models\Rating
     */
    public function ratings()
    {
        return $this->hasMany(Rating::class);
    }

    /**
     * Get the route key for the model
     *
     * @return string
     */
    public function getRouteKey()
    {
        return $this->alias;
    }

    /**
     * Get the average ratings for this post
     * @return int
     */
    public function getAverageRating(): int
    {
        return $this->ratings->avg->rating;
    }

    /**
     * The group type this tour belongs to
     * @return App\Models\GroupType
     */
    public function group_types()
    {
        return $this->belongsToMany(GroupType::class);
    }

    /**
     * The tags in this tour
     * @return App\Models\Tag
     */
    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    /**
     * The features this tour include
     * @return App\Models\Feature
     */
    public function features()
    {
        return $this->belongsToMany(Feature::class)
            ->withPivot('status');
    }

    /**
     * The location this tour belongs to
     * @return App\Models\Location
     */
    public function location()
    {
        return $this->belongsTo(Location::class);
    }

    /**
     * Get the featured image with the correct path
     *
     * @param string $val Featured Image path
     * @return string
     */
    public function getFeaturedImgAttribute($val)
    {
        if (is_null($val)) {
            return '/img/404.jpg';
        }

        return '/storage/' . $val;
    }

    /**
     * Get the features that this tour does not include
     *
     * @return App\Models\Feature
     */
    public function getNotFeaturesAttribute()
    {
        return $this->features()->wherePivot('status', 0)->get();
    }

    /**
     * Get the features that this tour does not include
     *
     * @return App\Models\Feature
     */
    public function getFeaturesAttribute()
    {
        return $this->features()->wherePivot('status', 1)->get();
    }

    /**
     * Get this tour day_times
     * @return \App\Models\DayTime
     */
    public function day_times()
    {
        return $this->belongsToMany(DayTime::class);
    }

    /**
     * Get the active days in number form --- 0-6 
     * @return array
     */
    public function getDaysNumbers()
    {
        return array_map(function($value){
            switch( strtolower($value) ){
                case 'sunday':
                $val = 0;
                break;
                case 'monday':
                $val = 1;
                break;
                case 'tuesday':
                $val = 2;
                break;
                case 'wednesday':
                $val = 3;
                break;
                case 'thursday':
                $val = 4;
                break;
                case 'friday':
                $val = 5;
                break;
                case 'saturday':
                $val = 6;
                break;
                default:
                $val = null;
                break;
            }
            return $val;
        }, $this->active_days);
    }

    /**
     * Get this tour days 
     * @return \App\Models\Day
     */
    public function days()
    {
        return $this->hasMany(Day::class);
    }

    /**
     * Get this tour days  (reservations)
     * @return \App\Models\Reservation
     */
    public function reservations()
    {
        return $this->hasMany(Reservation::class);
    }

    /**
     * The points in this tour
     * @return App\Models\Point
     */
    public function points()
    {
        return $this->belongsToMany(Point::class)
                    ->withPivot('order');
    }

    /**
     * Get this tours in order
     *
     * @return App\Models\Point
     */
    public function getPointsAttribute()
    {
        return $this->points()->get()->sortBy('pivot.order');
    }

    /**
     * Try to deactivate this tour
     * @return bool true if the tour is deactivate succesfully
     */
    public function deactivate()
    {
        if( ! $this->reservations->isEmpty() ){
            foreach( $this->reservations as $reservation){
                if ( !$reservation->isDeleteable() ){
                    return false;
                }
            }
        }
        $this->status = 0;
        $this->save();
        return true;
    }

    /**
     * Scope a query to only include active tours users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

}
