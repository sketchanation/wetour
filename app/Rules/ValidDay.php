<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

use Carbon\Carbon;
class ValidDay implements Rule
{

    protected $tour;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($tour)
    {
        $this->tour = $tour;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $date = new Carbon( $value );
        return (collect( $this->tour->getDaysNumbers() )->contains( $date->dayOfWeek )) ;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Please choose a valid day.'; // TODO hacer el mensaje de error
    }
}
