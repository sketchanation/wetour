<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

use App\Models\{Reservation, Tour};
use App\Observers\ReservationObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Schema::defaultStringLength(191);

        Reservation::observe(ReservationObserver::class);

        Tour::created(function($tour){
            $locales = config('app.locales');
            for ($i=0; $i <count($locales) ; $i++) { 
                $tour->setTranslation('alias', $locales[$i], $tour->alias);
            }
            $tour->save();
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
