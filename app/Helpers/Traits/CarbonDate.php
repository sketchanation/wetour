<?php

namespace App\Helpers\Traits;

use Carbon\Carbon;

/**
 * Trait to manage the conversion of date attribute into carbon instance and provide some helper functions
 */
trait CarbonDate
{
    /**
     * Get this day date in a carbon instace
     * @return \Carbon\Carbon
     */
    public function getCarbonDateAttribute()
    {
        return new Carbon ($this->date);
    }

    /**
     * Verify if this model date is after today
     * @return bool
     */
    public function hasPassed()
    {
        return ( now() >  $this->carbon_date );
    }

    /**
     * Get the a standar formatted date
     * @return string
     */
    public function getDateString()
    {
        return $this->carbon_date->toFormattedDateString();
    }
}