<?php

namespace App\Listeners;

use App\Events\GuideAssigned;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Notifications\GuideNotification;
use App\Notifications\CustomerNotification;

class SendEmailsNotifications
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  GuideAssigned  $event
     * @return void
     */
    public function handle(GuideAssigned $event)
    {
        $event->day->guide->user->notify(new GuideNotification($event->day, $event->comment));
        foreach($event->day->reservations->unique('user_id') as $reservation){
            $reservation->user->notify( new CustomerNotification($event->day, $event->comment) );
        }
    }
}
