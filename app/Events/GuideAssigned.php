<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use App\Models\Day;

class GuideAssigned
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * The group to notify
     * @var \App\Models\Day
     */
    public $day;

    /**
     * Any extra comments to be mailed
     * @var string|null
     */
    public $comment;

    /**
     * Create a new event instance.
     *
     * @param \App\Models\Day $day
     * @param string|null $comment
     * @return void
     */
    public function __construct(Day $day, $comment = null)
    {
        $this->day = $day;
        $this->comment = $comment;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
