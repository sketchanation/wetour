<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use App\Models\Day;

class GuideNotification extends Notification
{
    use Queueable;

    /**
     * The group to notify
     * @var \App\Models\Day
     */
    public $day;

    /**
     * Any extra comments to be mailed
     * @var string|null
     */
    public $comment;

    /**
     * Create a new notification instance.
     * 
     * @param \App\Models\Day $day
     * @param string|null $comment
     * @return void
     */
    public function __construct(Day $day, $comment = null)
    {
        $this->day = $day;
        $this->comment = $comment;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $comment = is_null($this->comment)? '' : "Some extra information your should know: {$this->comment}" ; 
        return (new MailMessage)
                    ->greeting('Hello! ' . $notifiable->name)
                    ->subject('You have a new Tour!')
                    ->line('You have been assigned a new Tour!!')
                    ->line('The tour is on ' . $this->day->getDateString() )
                    ->line('On the ' . $this->day->day_time->name )
                    ->line('In ' . $this->day->tour->location->name )
                    ->line($comment)
                    ->action('See this tour', route('tour.show', $this->day->tour))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
