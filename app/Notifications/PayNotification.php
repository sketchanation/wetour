<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PayNotification extends Notification
{
    use Queueable;

    /**
     * @var \App\Models\Reservation
     * The reservation paid
     */
    public $reservation;
    
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($reservation)
    {
        $this->reservation = $reservation;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->greeting('Hello! ' . $notifiable->name)
                    ->subject('You\'r payment has been made successfully!')
                    ->line("Your payment for the reservation of the \"{$this->reservation->tour->name}\" tour for {$this->reservation->persons} persons (Total) ")
                    ->line("On the {$this->reservation->getDateString()} in the {$this->reservation->day_time->name} ")
                    ->line("For a total of \${$this->reservation->price} (USD) ")
                    ->line("Your transaction id {$this->reservation->stripe_transaction_id} ")
                    ->line("Your pay method {$this->reservation->pay_method} ")
                    ->action('See more details', route('reservation.show', $this->reservation))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
