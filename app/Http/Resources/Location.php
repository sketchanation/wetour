<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\{Country as CountryResource, Tour as TourResource, Point as PointResource, UserGuide as UserGuideResource};

class Location extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'country_id' => $this->country_id,
            'img' => $this->img,
            'country' =>new CountryResource($this->whenLoaded('country')),
            'tours' => TourResource::collection($this->whenLoaded('tours')),
            'guides' => UserGuideResource::collection($this->whenLoaded('guides')),
            'points' => PointResource::collection($this->whenLoaded('points')),
            'tours' => TourResource::collection($this->whenLoaded('tours')),
            'created_at' => $this->created_at->toFormattedDateString(),
        ];
    }
}
