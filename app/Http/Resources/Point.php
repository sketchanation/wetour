<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\{Tour as TourResource, Location as LocationResource};

class Point extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'address' => $this->address,
            'img' => $this->img,
            'lat' => $this->lat,
            'lon' => $this->lon,
            'status' => $this->status,
            'location_id' => $this->location_id,
            'location' =>new LocationResource($this->whenLoaded('location')),
            'tours' => TourResource::collection($this->whenLoaded('tours')),
        ];
    }
}
