<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\{Reservation as ReservationResource, User as UserResource, UserGuide as UserGuideResource};

class Day extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "batch" => $this->batch,
            "date" => $this->capacity,
            "capacity" => $this->capacity,
            "full" => $this->full,
            "tour_id" => $this->tour_id,
            "user_guide_id" => $this->user_guide_id,
            "created_at" => $this->created_at->diffForHumans(),
            "day_time_id" => $this->day_time_id,
            'user' => new UserResource($this->whenLoaded('user')),
            'guide' => new UserGuide($this->whenLoaded('guide')),
            "reservations" => ReservationResource::collection($this->whenLoaded('reservations'))
        ];
    }
}
