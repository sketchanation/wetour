<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\{UserGuide as UserGuideResource, Role as RoleResource};

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'role' => $this->role,
            'created_at' => $this->created_at->diffForHumans(),
            'user_guide_id' => $this->user_guide_id,
            'guide' => new UserGuideResource($this->whenLoaded('guide')),
        ];
    }
}
