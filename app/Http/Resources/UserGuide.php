<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\{Location as LocationResource, User as UserResource};

class UserGuide extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'bio' => $this->bio,
            'rate' => $this->rate,
            'img' => $this->img,
            'phone' => $this->phone,
            'identification' => $this->identification,
            'location' => new LocationResource($this->whenLoaded('location')),
            'location_id' => $this->location_id,
            'user' => new UserResource($this->whenLoaded('user')),
        ];
    }
}
