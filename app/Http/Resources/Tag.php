<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\{Tour as TourResource, User as UserResource};

class Tag extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'tours' => TourResource::collection($this->whenLoaded('tours')),
            'users' => UserResource::collection($this->whenLoaded('users')),
        ];
    }
}
