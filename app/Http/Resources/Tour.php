<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\{Feature as FeatureResource,GroupType as GroupTypeResource, Tag as TagResource, Location as LocationResource, Point as PointResource, DayTime as DayTimeResource};

class Tour extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'alias' => $this->alias,
            'type' => $this->type,
            'featured_img' => $this->featured_img,
            'price' => $this->price,
            'description' => $this->description,
            'excerpt' => $this->excerpt,
            'max_age' => $this->max_age,
            'min_age' => $this->min_age,
            'active_days' => $this->active_days,
            'length' => $this->length,
            'status' => $this->status,
            'day_times' =>DayTimeResource::collection($this->day_times),
            'not_features' => FeatureResource::collection($this->not_features),
            'features' =>FeatureResource::collection($this->features),
            'group_types' =>GroupTypeResource::collection($this->whenLoaded('group_types')),
            'tags' =>TagResource::collection($this->whenLoaded('tags')),
            'location' =>new LocationResource($this->whenLoaded('location')),
            'location_id' =>$this->location_id,
            'points' =>PointResource::collection($this->whenLoaded('points')),
            // 'created_at' => $this->created_at,
            // 'updated_at' => $this->updated_at,
        ];
    }
}
