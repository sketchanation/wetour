<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\{User as UserResource, Tour as TourResource, DayTime as DayTimeResource, Day as DayResource};

class Reservation extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'pay_method' => $this->pay_method,
            'persons' => $this->persons,
            'day_id' => $this->day_id,
            'comment' => $this->comment,
            'created_at' => $this->created_at->diffForHumans(),
            'date' => $this->date,
            'price' => $this->price,
            'stripe_transaction_id' => $this->stripe_transaction_id,
            'day_time' => new DayTimeResource($this->whenLoaded('day_time')),
            'user' => new UserResource($this->whenLoaded('user')),
            'day' => new DayResource($this->whenLoaded('day')),
            'tour' => new TourResource($this->whenLoaded('tour')),
        ];
    }
}
