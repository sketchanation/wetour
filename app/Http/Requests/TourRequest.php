<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Validation\Rule;

class TourRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->hasRole('Admin');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            /**
             *  Attributes
             */
            'name' => 'required|string|between:1,150',
            'alias' => [
                'required',
                'string',
                Rule::unique('tours')->ignore($this->route('tour')),
            ],
            'type' => 'required|boolean',
            'featured_img' => 'nullable|string',
            'price' => 'required|numeric|min:1',
            'description' => 'required|string',
            'excerpt' => 'required|string',
            'min_age' => 'required|numeric|between:1,110',
            'max_age' => 'required|numeric|between:1,110|gte:min_age',
            // 'start_time' => 'required|string',
            'length' => 'required|numeric|min:1',
            // 'capacity' => 'required|numeric|min:1',
            'active_days' => 'required|array',

            /**
             * Relationships
             */
            'group_type' => 'required|array|exists:group_types,id',
            'country_id' => 'required|numeric|exists:countries,id',
            'location_id' => 'required|numeric|exists:locations,id',
            'tags' => 'present|array|exists:tags,id',
            'features' => 'present|array|exists:features,id',
            'not_features' => 'present|array|exists:features,id',
            'points' => 'present|array|exists:points,id',
            'day_times' => 'present|array|exists:day_times,id',

        ];
    }
}
