<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->hasRole('Admin');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            /**
             * Basic User
             */
            'name' => 'required|string|between:5,200',
            'email' => [
                'required',
                'string',
                'email',
                Rule::unique('users')->ignore($this->route('user')),
            ],
            'password' => [
                $this->isMethod('POST') ? 'required' : 'nullable',
                'string',
                'between:5,60',
            ],
            'role' => 'required|string|exists:roles,name',
            /**
             * Guide User
             */
            'guide_id' => 'sometimes|nullable|required|numeric|exists:user_guides,id',
            'bio' => 'required_if:role,Guide|string|between:5,100',
            'rate' => 'required_if:role,Guide|numeric|min:0',
            'img' => 'nullable|required_if:role,Guide|string',
            'phone' => 'required_if:role,Guide|string',
            'identification' => 'required_if:role,Guide|string',

            /**
             * Guide relationships
             */
            'location_id' => 'required_if:role,Guide|numeric|exists:locations,id',
            'country_id' => 'required_if:role,Guide|numeric|exists:countries,id',
        ];
    }
}
