<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PointRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->hasRole('Admin');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            /**
             *  Attributes
             */
            'name' => 'required|string|between:1,150',
            'description' => 'nullable|string',
            'img' => 'nullable|array',
            'address' => 'required|string',
            'lat' => 'required|numeric',
            'lon' => 'required|numeric',
            /**
             * Relationships
             */
            'country_id' => 'required|numeric|exists:countries,id',
            'location_id' => 'required|numeric|exists:locations,id',
        ];
    }
}
