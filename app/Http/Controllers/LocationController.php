<?php

namespace App\Http\Controllers;

use App\Models\{Location, Country};
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Khsing\World\World;

use App\Http\Resources\{Location as LocationResource, Country as CountryResource};

class LocationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'role:Admin'])->except('show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->has('id')) {
            return LocationResource::collection(Location::whereHas('country', function ($query) use ($request) {
                $query->where('id', $request->get('id'));
            })->get());

        }

        if ($request->has('name')) {
            return LocationResource::collection(Location::whereHas('country', function ($query) use ($request) {
                $query->where('name', $request->get('name'));
            })->get());
        }

        return LocationResource::collection(Location::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'country_id' => 'required|numeric|exists:world_countries,id',
            'name' => [
                'required',
                'string',
                Rule::unique('locations')->where(function($query) use ($request){
                    return $query->where('country_id', $request->country_id );
                }),
            ],
            'description' => 'sometimes|nullable|string|between:5,200',
            'img' => 'nullable',
        ]);
        $world = World::Countries()->firstWhere('id', $validated['country_id']);
        $country = Country::firstOrCreate([
            'id' => $world->id,
            'name' => $world->name,
            'code' => $world->code,
        ]);
        $location = Location::create([
            'name' => $validated['name'],
            'description' => array_has($validated, 'description') ? $validated['description'] : null,
            'country_id' => $world->id,
            'img' => array_has($validated, 'img') ? $validated['img'] : null,
        ]);

        $location->refresh();
        $location->load(['country']);
        return $this->show($location);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function show(Location $location)
    { 
        if (request()->ajax() && auth()->user()->hasRole('Admin') ) {
           return new LocationResource($location->load(['tours']));
        }

        return view('frontend.location',compact('location'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Location $location)
    {
        $validated = $request->validate([
            'country_id' => 'required|numeric|exists:world_countries,id',
            'name' => [
                'required',
                Rule::unique('locations')->where(function($query) use ($request){
                    return $query->where('country_id', $request->country_id );
                })->ignore($location),
            ],
            'description' => 'sometimes|nullable|string|between:5,200',
            'img' => 'nullable|string',
        ]);

        $world = World::Countries()->firstWhere('id', $validated['country_id']);

        $country = Country::firstOrCreate([
            'id' => $world->id,
            'name' => $world->name,
            'code' => $world->code,
        ]);

        $location->name = $validated['name'];
        if( array_has($validated,'description') )
        $location->description = $validated['description'];
        if( array_has($validated,'img') )
        $location->img = $validated['img'];
        $location->country()->associate($world->id);
        $location->save();
        $location->refresh();
        $location->load(['country']);

        return $this->show($location);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function destroy(Location $location)
    {
        $location->delete();
        return response()->json(null, 200);
    }

    /**
     * Display all the locations countries
     * @return App\Models\Country
     */
    public function indexCountries()
    {
        return CountryResource::collection(Country::whereHas('locations')->get());
    }
}
