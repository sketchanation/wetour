<?php

namespace App\Http\Controllers;

use App\Models\{Tour, GroupType, DayTime};
use App\Models\Location;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'role:Admin'])->only(['dashboard']);
    }

    /**
     * Show the application home.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tours = Tour::with(['tags', 'features', 'group_types'])->active()->get();
        $group_types = GroupType::all();
        $locations = Location::occupied();
        $times = DayTime::all();
        return view('frontend.index', compact(['tours','group_types','times','locations']));
    }

    /**
     * Show the application dashboard
     * 
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
        return view('dashboard.index');
    }

    /**
     * Show the dashboard login
     * 
     * @return \Illuminate\Http\Response
     */
    public function dashboardLogin()
    {
        return view('dashboard.login');
    }

}
