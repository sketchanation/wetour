<?php

namespace App\Http\Controllers;

use App\Http\Requests\TourRequest;
use App\Models\{GroupType, Location, Point, Tour, DayTime};
use Illuminate\Http\Request;

use App\Http\Resources\{Tour as TourResource};
use CodeZero\UniqueTranslation\UniqueTranslationRule;

class TourController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'role:Admin'])->except(['index', 'show','search']);
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $tours = Tour::all()->loadMissing(['tags', 'features', 'group_types', 'location', 'points']);
            return TourResource::collection($tours);
        }
        $tours = Tour::active()->get()->loadMissing(['tags', 'features', 'group_types', 'location', 'points']);
        $group_types = GroupType::all();

         $times =  DayTime::all();
         $max = Tour::max('price');
         $min = Tour::min('price');

        return view('frontend.tours', compact('tours', 'locations', 'group_types','max','min','times'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TourRequest $request)
    {
        $validated = $request->validated();
        $location = Location::find($validated['location_id']);

        $tour = Tour::create(array_except($validated, ['group_type', 'features', 'tags', 'country_id', 'not_features', 'points' , 'day_times']));

        $tour->location()->associate($location);

        $tour->tags()->attach($validated['tags']);
        $tour->day_times()->attach($validated['day_times']);
        $tour->group_types()->attach($validated['group_type']);

        foreach ($validated['features'] as $feature) {
            $tour->features()->attach($feature, ['status' => 1]);
        }

        foreach ($validated['not_features'] as $feature) {
            $tour->features()->attach($feature, ['status' => 0]);
        }

        foreach ($validated['points'] as $key => $point) {
            $tour->points()->attach($point, ['order' => $key + 1]);
        }

        $tour->save();

        return $this->show($tour);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tour  $tour
     * @return \Illuminate\Http\Response
     */
    public function show(Tour $tour)
    {
        $tour->load(['tags', 'features', 'group_types', 'location', 'points','day_times']);
        return request()->ajax() ? new TourResource($tour) : view('frontend.tour', compact('tour'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tour  $tour
     * @return \Illuminate\Http\Response
     */
    public function update(TourRequest $request, Tour $tour)
    {
        $validated = $request->validated();
        $location = Location::find($validated['location_id']);
        $tour->fill(array_except($validated, ['group_type', 'features', 'tags', 'country_id', 'location_id', 'not_features', 'points', 'day_times']));

        $tour->tags()->sync($validated['tags']);
        $tour->day_times()->sync($validated['day_times']);
        $tour->group_types()->sync($validated['group_type']);

        $tour->location()->associate($location);

        $tour->features()->detach();
        foreach ($validated['features'] as $feature) {
            $tour->features()->attach($feature, ['status' => 1]);
        }

        foreach ($validated['not_features'] as $feature) {
            $tour->features()->attach($feature, ['status' => 0]);
        }

        $tour->points()->detach();
        $points = Point::find($validated['points']);
        $i = 0;
        foreach ($points as $point) {
            if ($point->location_id == $validated['location_id']) {
                $i++;
                $tour->points()->attach($point, ['order' => $i]);
            }
        }
        $tour->save();
        return $this->show($tour);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tour  $tour
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tour $tour)
    {
        $tour->delete();

        return response()->json(null, 200);
    }

    /**
     * Activate this tour
     * @param \App\Models\Tour
     * @return json
     */
    public function activate(Tour $tour)
    {
        $tour->status = true;

        $tour->save();

        return response()->json(null, 200);
    }

    /**
     * Deactivate this tour
     * @param \App\Models\Tour
     * @return json
     */
    public function deactivate(Tour $tour)
    {
        if ($tour->deactivate()) {
            return response()->json(null, 200);
        } else {
            return abort(422, 'This tour cannot be deactivated because it has upcomings reservations');
        }

    }

    /**
     * search
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return void
     */
    public function search(Request $request)
    {
        $validated = $request->validate([
            'type' => 'nullable|numeric|exists:group_types,id',
            'search' => 'nullable|string',
            'day_time' => 'nullable|numeric|exists:day_times,id',
            'price' => 'sometimes',
        ]);
        $price = null;
        if(isset($validated['price'])){
            $price = explode(
                ',', //parse min and max price
                str_replace('$','', $validated['price'])
            );
        }
        $tours = Tour::active()->whereHas('group_types', function ($query) use ($validated) {
            if(isset($validated['type'])){
                $query->where('group_types.id', $validated['type']);
            }else {
                return;
            }
          })
          ->whereHas('day_times', function($query) use ($validated) {
            if(isset($validated['day_time'])){
                $query->where('day_times.id', $validated['day_time']);
            }else {
                return;
            } 
          })
            ->when(isset($validated['search']), function ($q) use ($validated) {
                return $q->where('name', 'like', '%' . $validated['search'] . '%');
            })
            ->when(isset($validated['price']), function ($q) use ($price) {
                return    $q->where('price','>=', $price[0] )
                            ->where('price','<=', $price[1] );
            })
            ->paginate(12);
            
        //load neccesary data
         $tours->load(['location', 'group_types']);
         $group_types = GroupType::all();
         $times =  DayTime::all();
         $max = Tour::max('price');
         $min = Tour::min('price');

         return view('frontend.searchResults',compact(['tours','group_types','times','max','min']));
    }

    /**
     * Return Tour's Translatable Information 
     * 
     * @param \App\Models\Tour $tour
     */
    public function translate(Tour $tour){
        return $tour->getTranslations();
    }

    /**
     * Save Tour's Translation 
     * 
     * @param  \Illuminate\Http\Request  $request
     * @param \App\Models\Tour $tour
     */
    public function saveTranslate(Request $request,Tour $tour){
        
        $validated = $request->validate([
            'name.*' => [
                'nullable',
                'string',
                'between:1,100',
                UniqueTranslationRule::for('tours')->ignore($tour->id)
            ],
            'description.*' => [
                'nullable', 
                'between:1,100',
                'string', 
                UniqueTranslationRule::for('tours')->ignore($tour->id) ],
            'excerpt.*' => [
                'nullable', 
                'between:1,100',
                'string', 
                UniqueTranslationRule::for('tours')->ignore($tour->id) ],
            'alias.*' => [
                'nullable', 
                'string', 
                UniqueTranslationRule::for('tours')->ignore($tour->id) ],
        ]);

            
        foreach ($validated as $key => $t) {
            $tour->setTranslations($key,$t);
        }
        $tour->save();

        return $this->translate($tour);
    }
}
