<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use App\Models\Tag;

class CustomerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'role:Customer']);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $user = auth()->user();
        return view('frontend.customer.index',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $user = auth()->user();

        $tags = Tag::all();

        return view('frontend.customer.edit',compact(['tags','user']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = auth()->user();
        $validated = $request->validate([
            'name' => 'present|string|between:1,100',
            'email' => ['present','string','email', Rule::unique('users')->ignore($user)],
            'password' => 'present|confirmed',
            'likes' => 'present|array|exists:tags,id',
        ]);
    
        $user->fill(array_except($validated,'password'));
        if(isset($validated['password'])){
            $user->password = Hash::make($validated['password']);
        }
        $user->tags()->sync($validated['likes']);

        $user->save();
        return $this->show();
    }

}
