<?php

namespace App\Http\Controllers;

use App\Models\Point;
use Illuminate\Http\Request;
use App\Http\Requests\PointRequest;
use App\Http\Resources\{Point as PointResource};


class PointController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'role:Admin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if( request()->has('location_id') )
        $points = Point::where('location_id', request()->location_id)->get();
        else 
        $points = Point::with(['tours', 'location'])->get();
        return PointResource::collection($points);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PointRequest $request)
    {
        $validated = $request->validated();

        $point = Point::create(array_except($validated, ['country_id']));

        return $this->show($point);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Point  $point
     * @return \Illuminate\Http\Response
     */
    public function show(Point $point)
    {
        return new PointResource($point->loadMissing(['location', 'tours']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Point  $point
     * @return \Illuminate\Http\Response
     */
    public function update(PointRequest $request, Point $point)
    {
        $validated = $request->validated();

        $point->fill(array_except($validated, ['country_id']));

        $point->save();
        
        return $this->show($point);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Point  $point
     * @return \Illuminate\Http\Response
     */
    public function destroy(Point $point)
    {
        if ( $point->tours->isEmpty() ){
            $point->delete();
            return response()->json(null, 200);
        } else {
            return abort(422, 'This point cannot be deleted because it has tours associated!');
        }
    }
}
