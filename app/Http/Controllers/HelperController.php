<?php

namespace App\Http\Controllers;

use App\Models\Tour;
use Illuminate\Http\Request;
use Khsing\World\World;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\App;
use App\Http\Resources\Role as RoleResource;


class HelperController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'role:Admin'])->except(['locale']);
    }

    /**
     * Helper url to slugify a tour name to be a url-friendly unique slug
     * @return array
     */
    public function slugify()
    {
        $name = request('name');
        $id = request('id');
        $slug = str_slug($name);
        if( ! is_null($id) )
        $tour = Tour::findOrFail($id);
        $tours = Tour::all();
        while ( !is_null($tours->where('alias', $slug)->first())  ) {
           if( ! is_null($id) ){
               if( $tour->alias == $slug)
               break;
           }
            $name .= '-1';
            $slug = str_slug($name);
        };

        return response()->json([
            'data' => [
                'name' => $slug,
            ]
        ]);
    }

    /**
     * Helper action to load a image
     * @return string|null
     */
    public function uploadImage()
    {

        if (request()->hasFile('image') and request()->file('image')->isValid()) {
            $path = request()->file('image')->store('img/tour/featured');
            return $path;
        }

        if (request()->hasFile('avatar') and request()->file('avatar')->isValid()) {
            $path = request()->file('avatar')->store('img/avatar');
            return $path;
        }

        if (request()->hasFile('point') and request()->file('point')->isValid()) {
            $path = request()->file('point')->store('img/point');
            return $path;
        }

        if (request()->hasFile('location') and request()->file('location')->isValid()) {
            $path = request()->file('location')->store('img/location');
            return $path;
        }

        return response()->json(null, 501);
    }

    /**
     * Helper action to get all the registered countries
     * @return array
     */
    public function worldCountries()
    {
        return response()->json([
            'data' => World::Countries()
        ]);
    }

    /**
     * Helper action to get all de roles
     * @return array
     */
    public function roles()
    {
        return RoleResource::collection(Role::all());
    }

    /**
     * Change the application locale
     */
    public function locale($locale)
    {
        if ( in_array($locale, config('app.locales')) ) {
          session(['locale' => $locale]);
          app()->setLocale($locale);
        }
        if(session()->has('tour_id')){
        return redirect()->route('tour.show',Tour::find(session('tour_id')));
        }
        return redirect()->back();
    }

    /**
     * Provide Application Locales
     * @return array
     */
    public function locales()
    {
        return config('app.locales');
    }
    
    // /**
    //  * Provisional action for development to test json structures
    //  */
    // public function test()
    // {
    //     return;
    // }
}
