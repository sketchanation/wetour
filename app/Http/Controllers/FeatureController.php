<?php

namespace App\Http\Controllers;

use App\Models\Feature;
use Illuminate\Http\Request;

use Illuminate\Validation\Rule;
use CodeZero\UniqueTranslation\UniqueTranslationRule;

use App\Http\Resources\{Feature as FeatureResource};

class FeatureController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'role:Admin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return FeatureResource::collection(Feature::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => [
                'required',
                'string',
                'between:1,100',
                UniqueTranslationRule::for('features','name')
            ],
            'description' => 'nullable|string|between:1,200',
        ]);

        $feature = Feature::create($validated);
        return $this->show($feature);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Feature  $feature
     * @return \Illuminate\Http\Response
     */
    public function show(Feature $feature)
    {
        return new FeatureResource($feature);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Feature  $feature
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Feature $feature)
    {
        $validated = $request->validate([
            'name' => [
                'required',
                'string',
                'between:1,100',
                UniqueTranslationRule::for('features','name')->ignore($feature->id)
            ],
            'description' => 'nullable|string|between:1,200',
        ]);

        $feature->fill($validated);
        $feature->save();
        return $this->show($feature);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Feature  $feature
     * @return \Illuminate\Http\Response
     */
    public function destroy(Feature $feature)
    {
        $feature->delete();
        return response()->json(null, 200);

    }
}
