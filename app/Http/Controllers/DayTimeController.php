<?php

namespace App\Http\Controllers;

use App\Models\DayTime;
use Illuminate\Http\Request;

use App\Http\Resources\{DayTime as DayTimeResource};
use Illuminate\Validation\Rule;

class DayTimeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'role:Admin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return DayTimeResource::collection(DayTime::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|string|between:1,100|unique:day_times,name',
            'description' => 'nullable|string|between:1,200',
        ]);

        $daytime = DayTime::create($validated);
        return $this->show($daytime);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DayTime  $dayTime
     * @return \Illuminate\Http\Response
     */
    public function show(DayTime $dayTime)
    {
        return new DayTimeResource($dayTime);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DayTime  $dayTime
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DayTime $dayTime)
    {
        $validated = $request->validate([
            'name' => [
                'required',
                'string',
                'between:1,100',
                Rule::unique('day_times')->ignore($dayTime)
            ],
            'description' => 'nullable|string|between:1,200',
        ]);

        $dayTime->fill($validated);
        $dayTime->save();
        return $this->show($dayTime);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DayTime  $dayTime
     * @return \Illuminate\Http\Response
     */
    public function destroy(DayTime $dayTime)
    {
        $dayTime->delete();
        return response()->json(null, 200);
    }
}
