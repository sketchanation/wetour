<?php

namespace App\Http\Controllers;

use App\Models\{Reservation, Tour, Day};
use App\User;
use Illuminate\Http\Request;

use Illuminate\Validation\Rule;
use App\Rules\ValidDay;

use App\Http\Resources\{Reservation as ReservationResource, Tour as TourResource, DayTime as DayTimeResource};

use Stripe;
class ReservationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth']);
        $this->middleware('role:Admin')->only(['destroy', 'cancel', 'refund']);
        $this->middleware('role:Customer')->only(['book', 'showPreview', 'store']);
        $this->middleware('role:Admin|Customer')->only(['index', 'show', ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax() and auth()->user()->hasRole('Admin')) {

            if(request('date')){
                return ReservationResource::collection(Reservation::where('date',request('date'))->get());
            }

            if(request('tour')){
                $reservations = Reservation::where('tour_id', request('tour'))->get(); 
                $tour = Tour::active()->firstOrFail(request('tour'))->load('location'); 
                return response()->json([
                    'reservations' => ReservationResource::collection($reservations) ,
                    'day_times' => DayTimeResource::collection($reservations->pluck('day_time')->unique()),
                    'dates' => $reservations->pluck('date')->unique()->flatten()->all(),
                    'tour' => new TourResource($tour),
                ]);
            }

            return ReservationResource::collection(Reservation::withTrashed()->with('tour')->get());
        }
        abort_if(auth()->user()->hasRole('Admin'), 404);

        return view('frontend.reservation.index');
    }

    /**
     * Preview the payment.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param \App\Models\Tour
     * @return \Illuminate\Http\Response
     */
    public function book(Request $request, Tour $tour)
    {
        $validated = $request->validate([
            'date' => [
                'required',
                'date',
                'after:today',
                new ValidDay($tour)
            ],
            'day_time' => [
                'required',
                'exists:day_times,id',
                Rule::in($tour->day_times->pluck('id')->all())
            ],
            'persons' => 'required|numeric|min:1',
        ]);

        $user = auth()->user();
        $price = $tour->price * $validated['persons'];

        session(array_merge($validated,[
            'price' => $price,          
        ]));

        return redirect()->route('reservation.preview', $tour);
    }

    public function showPreview(Tour $tour)
    {
        if(session()->has(['date', 'day_time', 'persons', 'price']))
        return view('frontend.reservation.preview',compact('tour'));
        else
        return redirect()->route('tour.show', $tour);
    }

    /**
     * Create the reservation.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param \App\Models\Tour
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Tour $tour)
    {
        $validated = $request->validate([
            'comment' => 'nullable|string',
            'terms' => 'accepted',
            'stripeToken' => 'required|string',
            'extras_name' => 'sometimes|nullable|required|array',
            'extras_email' => 'sometimes|nullable|required|array',
            'extras_email.*' => 'sometimes|nullable|required|email',
            'extras_age' => 'sometimes|nullable|required|array',
        ]);
        $request->flash();        
        if (array_has($validated, 'extras_age')){
            $age = collect($validated['extras_age']);
            if( $age->min() < $tour->min_age or $age->max() > $tour->max_age ){
                return back()->withErrors('Some of your accompanions does not meet the age requirements for this tours.');
            }
        }
        
        if( !session()->has(['date', 'day_time', 'persons', 'price']))
        return redirect()->route('tour.show', $tour)->withErrors('Something went wrong');

        $user = auth()->user();
        
        try {
            $stripe = Stripe::make();
            // Verify if Something went wrong creating the token
            if ( !isset($validated['stripeToken']) ) {
                return back()->withErrors('Somenthing went wrong, verify your information and try again');
            }
            $token = $validated['stripeToken'];

            $persons = session()->pull('persons');
            $day_time = session()->pull('day_time');
            $date = session()->pull('date');
            $price = session()->pull('price');

            $charge = $stripe->charges()->create([
                'card' => $token,
                'currency' => 'USD',
                'amount' => $price,
                'description' => "Reserved by {$user->name} ({$user->email}) for the '{$tour->name}' tour, {$persons} persons, date: {$date}, time: {$day_time} ", 
            ]);

            if ( $charge['status'] == 'succeeded' ) {
                
                $reservation = Reservation::create([
                    'pay_method' => 'credit card',
                    'stripe_transaction_id' => $charge['id'],
                    'persons' => $persons,
                    'price' => $charge['amount'],
                    'day_time_id'=> $day_time,
                    'date'=> $date,
                    'comment'=> array_has($validated, 'comment') ? $validated['comment'] : null,
                    'user_id' => $user->id,
                    'tour_id' => $tour->id,
                ]);
                
                if (array_has($validated, ['extras_name', 'extras_email', 'extras_age'])) {
                    $data = array();
                    $size = collect([
                        count($validated['extras_name']),
                        count($validated['extras_email']),
                        count($validated['extras_age'])
                    ])->min();
                    for ($i=0; $i < $size ; $i++) { 
                        array_push($data,[
                            'name' => $validated['extras_name'][$i],
                            'email' => $validated['extras_email'][$i],
                            'age' => $validated['extras_age'][$i],
                        ]);
                    }
                    $reservation->extras()->createMany($data);
                }

            }
        } catch (Exception $e) {
            return back()->withErrors($e->getMessage());
        } catch (\Cartalyst\Stripe\Exception\CardErrorException $e) {
            return back()->withErrors($e->getMessage());
        } catch (\Cartalyst\Stripe\Exception\MissingParameterException $e) {
            return back()->withErrors($e->getMessage());
        } 

        return redirect()->route('reservation.show', $reservation)->withSuccess('Payment made successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function show(Reservation $reservation)
    {
        if (request()->ajax() and auth()->user()->hasRole('Admin')) {
            return new ReservationResource($reservation->load('tour'));
        }
        if (auth()->user()->hasRole('Admin') or $reservation->user->is(auth()->user()) ) {
            return view('frontend.reservation.show', compact('reservation'));
        }
        return abort(404);

    }

    /**
     * Cancel this reservations.
     *
     * @param  \App\Models\Reservation  $reservation
     * @return mixed
     */
    public function cancel(Day $day)
    {
        $reservations = $day->reservations;

        $result = $reservations->map(function ($item, $key) {
            return $this->refund($item);
        });
        $day->guide()->dissociate();
        $day->save();
        $day->delete();

        return $result;
    }


    /**
     * Refund this reservation.
     *
     * @param  \App\Models\Reservation  $reservation
     * @return mixed
     */
    public function refund(Reservation $reservation)
    {
        try {

            $stripe = Stripe::make();
        
            if ( $reservation->trashed() )
            return abort(422, 'This reservation already has been refunded');

            $refund = $stripe->refunds()->create( $reservation->stripe_transaction_id );

            $reservation->refund_id = $refund['id'];

            $reservation->day()->dissociate();
            $reservation->save();
            $reservation->delete();

        } catch (Exception $e) {
            return back()->withErrors($e->getMessage());
        }

        return $refund;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reservation $reservation)
    {
        $reservation->forceDelete();
        return response()->json(null, 200);

    }
}
