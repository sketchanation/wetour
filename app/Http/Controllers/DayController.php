<?php

namespace App\Http\Controllers;

use App\Models\{Day, Tour, Reservation, UserGuide as Guide};
use Illuminate\Http\Request;

use App\Events\GuideAssigned;
use App\User;

use App\Http\Resources\{Day as DayResource};


class DayController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'role:Admin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request()->has(['day_time','date','tour'])){
            $day = Day::with(['guide', 'reservations', 'day_time'])->where([
                ['day_time_id',request('day_time')],
                ['date',request('date')],
                ['tour_id', request('tour')]
            ])->get(); 
            return DayResource::collection($day);
        }

        return DayResource::collection(Day::with(['tour', 'guide', 'day_time'])->get());
    }

    /**
     * Create or update the days massive 
     * @return mixed
     */
    public function massiveSave(Request $request, Tour $tour)
    {
        $validated = $request->validate([
            'groups' => 'present|array',
            'groups.*.id' => 'sometimes|exists:days,id',
            'groups.*.reservations.id' => 'sometimes|present|exists:reservations,id',
        ]);
        $groups = $validated['groups'];
        $response = array();
        for ($i=0; $i < count($groups) ; $i++) {
            $reservations = Reservation::find(array_pluck($groups[$i]['reservations'], 'id') );
            
            if ($reservations->isEmpty() ) {
                if(array_has($groups[$i], 'id')){
                    $day = Day::find($groups[$i]['id']);
                    $day->reservations->each(function ($item, $key) {
                        $item->day()->dissociate();
                        $item->save();
                    });
                    $day->delete();
                }
            } else {
                if (array_has($groups[$i], 'id')) {
                    $day = Day::find($groups[$i]['id']);

                    $this->validateReservations($reservations, $tour, $day->date, $day->day_time_id);

                    $day->fill([
                        'capacity' => $reservations->sum->persons,
                    ]);
            
                    $day->reservations->each(function ($item, $key) {
                        $item->day()->dissociate();
                        $item->save();
                    });
                    $reservations->each(function ($item, $key) {
                        $item->day()->dissociate();
                        $item->save();
                    });
                    $day->reservations()->saveMany($reservations);
                } else {
                    $day_time_id = $reservations->first()->day_time_id;
                    $date = $reservations->first()->date;

                    $validReservations = $this->validateReservations($reservations, $tour, $date, $day_time_id);

                    $batch = Day::where([
                        'day_time_id' => $day_time_id,
                        'date' => $date,
                        'tour_id' => $tour->id,
                    ])->count();
            
                    $day = Day::create([
                        'day_time_id' => $day_time_id,
                        'date' => $date,
                        'capacity' => $reservations->sum->persons,
                        'tour_id' => $tour->id,
                        'batch' => $batch,
                    ]);
            
                    $day->reservations()->saveMany($reservations);
                }
                
                $day->load(['reservations', 'guide']);
                array_push($response, array_merge($day->toArray() , ['show' => $groups[$i]['show']]));
            }

        }

        return $response;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Day  $day
     * @return \Illuminate\Http\Response
     */
    public function mail(Request $request, Day $day)
    {
        $validated = $request->validate([
            'confirmation' => 'required|accepted',
            'comment' => 'nullable|string',
        ]);
            $comment = array_has($validated, ['comment'])? $validated['comment'] : null;
            event(new GuideAssigned($day, $comment));        
        return response()->json(200);
    }

    /**
     * Validate a collection of reservations
     * Check that all the reservations are in the same day, time of day and tour
     * @param \App\Models\Reservation $reservations
     * @param \App\Models\Tour $tour
     * @param string $date
     * @param string $day_time_id
     * @return void
     */
    public function validateReservations($reservations, $tour, $date, $day_time_id)
    {
        $validReservations =  $reservations->every(function($value, $key) use ($date, $day_time_id, $tour) {
            return  ($value->date == $date) and ($value->day_time_id == $day_time_id) and ($value->tour_id == $tour->id);
        });
        abort_unless( $validReservations, 422, 'This reservations have diferent date or day time');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Day  $day
     * @return \Illuminate\Http\Response
     */
    public function show(Day $day)
    {
        return new DayResource($day);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Day  $day
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Day $day)
    {
        $validated = $request->validate([
            'reservations' => 'present|array|exists:reservations,id',
            'user_guide_id' => 'sometimes|nullable|exists:user_guides,id',
        ]);
        $reservations = Reservation::find($validated['reservations']);

        $this->validateReservations($reservations, $day->tour, $day->date, $day->day_time_id);

        $day->fill([
            'capacity' => $reservations->sum->persons,
        ]);

        $day->reservations->each(function ($item, $key) {
            $item->day()->dissociate();
            $item->save();
        });
        $reservations->each(function ($item, $key) {
            $item->day()->dissociate();
            $item->save();
        });

        $day->reservations()->saveMany($reservations);

        if ( array_has($validated, 'user_guide_id') ) {
            $guide = Guide::find($validated['user_guide_id']);
            $day->guide()->associate($guide);
        }

        $day->save();
        $day->load('reservations');
        return $this->show($day);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Day  $day
     * @return \Illuminate\Http\Response
     */
    public function destroy(Day $day)
    {
        if( $day->isDeleteable() ){
            $day->delete();
            return response()->json(null,200);
        } else {
            return abort(422, "Can't delete this reservation before it happens.");
        }
    }
}
