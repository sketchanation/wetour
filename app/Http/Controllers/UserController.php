<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\UserGuide;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Resources\{UserGuide as UserGuideResource, User as UserResource};

class UserController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'role:Admin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return UserResource::collection(User::all()->loadMissing('guide'));
    }

    /**
     * Display all the guides
     *
     * @return \Illuminate\Http\Response
     */
    public function indexGuides()
    {
        if (request()->has('location_id')) {
            
            return UserGuideResource::collection(UserGuide::with('user')->where('location_id', request('location_id'))->get());
        }
        return UserGuideResource::collection(UserGuide::all()->loadMissing('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\UserRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $validated = $request->validated();

        $user = User::create([
            'name' => $validated['name'],
            'email' => $validated['email'],
            'password' => Hash::make($validated['password']),
        ]);

        if ($validated['role'] == 'Guide') {
            $guide = UserGuide::create([
                'bio' => $validated['bio'],
                'rate' => $validated['rate'],
                'img' => array_has($validated, 'img') ? $validated['img'] : null,
                'phone' => $validated['phone'],
                'identification' => $validated['identification'],
                'user_id' => $user->id,
                'location_id' => $validated['location_id'],
            ]);
            $user->user_guide_id = $guide->id;
            $user->save();
        }

        $user->assignRole($validated['role']);
        $user->refresh();
        $user->load('guide');
        return $this->show($user);;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return new UserResource($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UserRequest  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, User $user)
    {
        $validated = $request->validated();

        if ( $user->hasRole('Guide') and ! $user->checkGuide() and $validated['role'] != 'Guide' )
        return abort(422, "This user can't change roles because he has tours assigned ");
        
        if ($validated['role'] == 'Guide') {

            if (!is_null($user->guide)) {
                $guide = UserGuide::find($user->guide->id);
            } else {
                $guide = new UserGuide;
            }
            $guide->fill([
                'bio' => $validated['bio'],
                'rate' => $validated['rate'],
                'img' => array_has($validated, 'img') ? $validated['img'] : null,
                'phone' => $validated['phone'],
                'identification' => $validated['identification'],
                'user_id' => $user->id,
                'location_id' => $validated['location_id'],
            ]);
            $guide->save();
            $user->user_guide_id = $guide->id;
        } else {
            if (!is_null($user->guide)) {
                $guide = $user->guide;
                $guide->user()->dissociate();
                $user->user_guide_id = null;
                $user->save();
                $guide->save();
                $guide->delete();
            }
        }

        $user->fill([
            'name' => $validated['name'],
            'email' => $validated['email'],
        ]);
        
        if ( array_has($validated, 'password') ){
            $user->password = Hash::make($validated['password']); 
        }
        
        $user->save();

        $user->syncRoles($validated['role']);
        $user->refresh();
        $user->load('guide');
        return $this->show($user);;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if ( ! $user->checkGuide() )
        abort(422, 'This user cant be deleted because he has tours assigned');
        if( auth()->user()->is($user) )
        abort(422, 'You can delete yourself');

        /**
         * Check if the user is guide and gracefully break the relationship and delete both models
         */
        if( $user->hasRole('Guide') ){
            $guide = $user->guide;
            $guide->user()->dissociate();
            $guide->save();
            $user->user_guide_id = null;
            $user->save();
            $guide->delete();
        }
        $user->delete();
        return response()->json(null, 200);
    }

    /**
     * Show the authenticad user
     */
    public function showMe()
    {
        return new UserResource(auth()->user()->loadMissing(['guide', 'roles']));
    }
}
