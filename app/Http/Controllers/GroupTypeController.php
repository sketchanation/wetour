<?php

namespace App\Http\Controllers;

use App\Models\GroupType;
use Illuminate\Http\Request;
use App\Http\Resources\{GroupType as GroupTypeResource};
use Illuminate\Validation\Rule;
use CodeZero\UniqueTranslation\UniqueTranslationRule;

class GroupTypeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'role:Admin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return GroupTypeResource::collection(GroupType::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => [
                'required',
                'string',
                'between:1,100',
                UniqueTranslationRule::for('group_types','name')
            ],
            'description' => 'nullable|string|between:1,200',
        ]);
        $grouptype = GroupType::create($validated);
        return $this->show($grouptype);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\GroupType  $grouptype
     * @return \Illuminate\Http\Response
     */
    public function show(GroupType $grouptype)
    {
        return new GroupTypeResource($grouptype);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\GroupType  $grouptype
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GroupType $grouptype)
    {
        $validated = $request->validate([
            'name' => [
                'required',
                'string',
                'between:1,100',
                UniqueTranslationRule::for('group_types','name')->ignore($grouptype->id)
            ],
            'description' => 'nullable|string|between:1,200',
        ]);
        $grouptype->fill($validated);
        $grouptype->save();

        return $this->show($grouptype);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\GroupType  $grouptype
     * @return \Illuminate\Http\Response
     */
    public function destroy(GroupType $grouptype)
    {
        $grouptype->delete();
        return response()->json(null, 200);
    }
}
