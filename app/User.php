<?php

namespace App;

use App\Models\{Reservation, UserGuide as Guide, Tour, Tag};

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the roles of this user
     * @return array|\Spatie\Permission\Models\Role
     */
    public function getRoleAttribute()
    {
        return $this->getRoleNames()->first();
    }

    /**
     * Get the user guide information
     * @return \App\Models\UserGuide
     */
    public function guide()
    {
        return $this->hasOne(Guide::class, 'id', 'user_guide_id');
    }

    /**
     * Get this user reservations
     * @return \App\Models\Reservation
     */
    public function reservations()
    {
        return $this->hasMany(Reservation::class);
    }

    /**
     * The tags in this tour
     * @return App\Models\Tag
     */
    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    /**
     * Get the reserved tours for this user
     * @param bool $old true to include passed reservations
     * @return \App\Models\Tour
     */
    public function getReservedTours($old = false)
    {
        if ($old)
        return $this->reservations->each->tour->pluck('tour')->unique();

        return $this->getNextReservations()->each->tour->pluck('tour')->unique();
    }

    /**
     * Get the upcoming reservations for this user
     * @return \App\Models\Tour
     */
    public function getNextReservations()
    {
        return $this->reservations->filter(function ($value, $key) {
            return !$value->hasPassed();
        })->sortBy('carbon_date');
    }

    /**
     * Get the reserved tours for this user
     * @param \App\Models\Tour
     * @return \App\Models\Reservation
     */
    public function getTourReservations($tour)
    {
        return $this->getNextReservations()->where('tour_id',$tour->id);
    }

    /**
     * Get the most recent reservation for this user
     * @param \App\Models\Tour|null
     * @return \App\Models\Reservation
     */
    public function getRecentReservation($tour = null)
    {

        return is_null( $tour ) ? $this->getClosestReservation() : $this->getTourReservations($tour)->first();
    }

    /**
     * Get the closest reservation to today
     * @return \App\Models\Reservation
     */
    public function getClosestReservation()
    {
        return $this->getNextReservations()->first();
    }
    
    /**
     * Check if this user guide information can be destroyed
     * @return bool true if the guide CAN be deleted
     */
    public function checkGuide()
    {
        return is_null( $this->guide ) ?: $this->guide->checkDays();
    }
}
