<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', 'HomeController@index')->name('home');

Auth::routes();


//Admin Panel
Route::group([
    'prefix' => 'dashboard'
    ], function () {
        Route::get('login', 'HomeController@dashboardLogin')->middleware('guest')->name('dashboard.login');
        Route::get('/', 'HomeController@dashboard')->name('dashboard');
        Route::get('{any}', 'HomeController@dashboard')->where('any', '.*');
});
// End Admin Panel

//  Helper Controller Routes
// Route::get('test', 'HelperController@test')->name('test');

Route::get('setlocale/{locale}', 'HelperController@locale')->name('locale');

Route::get('slugify', 'HelperController@slugify')->name('slugify');

Route::post('upload/image', 'HelperController@uploadImage')->name('upload.image');

Route::get('world/countries', 'HelperController@worldCountries')->name('world.countries');

Route::get('roles', 'HelperController@roles')->name('roles.index');
Route::get('locales', 'HelperController@locales')->name('locales.index');

// End Helper Controller Routes

//Customer Routes
Route::get('profile', 'CustomerController@show')->name('profile');
Route::get('profile/edit', 'CustomerController@edit')->name('profile.edit');
Route::put('profile', 'CustomerController@update')->name('profile.update');
// End Customer Routes

//Day Extra Routes
Route::post('{tour}/day/groupsave', 'DayController@massiveSave' )->name('day.massive');
Route::post('day/{day}/mail', 'DayController@mail' )->name('day.mail');
// End Day Extra Routes

//Location Extra Routes
Route::get('country', 'LocationController@indexCountries')->name('country.index');
// End Location Extra Routes

//Reservation Extra Routes
Route::get('preview/{tour}', 'ReservationController@showPreview')->name('reservation.preview');

Route::post('reservation/{tour}', 'ReservationController@store')->name('reservation.pay');
Route::post('tour/{tour}/book', 'ReservationController@book')->name('tour.book');

Route::delete('day/{day}/cancel', 'ReservationController@cancel')->name('day.cancel');
Route::delete('{reservation}/refund/', 'ReservationController@refund')->name('reservation.refund');
//End Reservation Extra Routes

//User Extra Routes
Route::get('guide', 'UserController@indexGuides')->name('guide.index');
Route::get('me', 'UserController@showMe')->name('user.me');
//End User Extra Routes

//Tour Extra Routes
Route::get('tour/search', 'TourController@search')->name('search');
Route::get('tour/{tour}/translate', 'TourController@translate')->name('tour.translate');

Route::post('tour/{tour}/translate', 'TourController@saveTranslate')->name('tour.savetranslate');
Route::post('tour/{tour}/activate', 'TourController@activate')->name('tour.activate');
Route::post('tour/{tour}/deactivate', 'TourController@deactivate')->name('tour.deactivate');
//End Tour Extra Routes


/**
 * Resourcefull-ish routes group by controller/resource
 */
//Resource Routes
// Route::resource('rating', 'RatingController');

//End Resource Routes

// ApiResource Routes
Route::apiResources([
    'day' => 'DayController',
    'day-time' => 'DayTimeController',
    'feature' => 'FeatureController',
    'grouptype' => 'GroupTypeController',
    'location' => 'LocationController',
    'point' => 'PointController',
    'tag' => 'TagController',
    'user' => 'UserController',
    'tour' => 'TourController',
]);

Route::apiResource('reservation','ReservationController')->except([
    'store', 'update'
]);
//End ApiResource Routes
